const webpack = require('webpack');
const path = require('path');
const buildPath = path.resolve(__dirname, 'build');
const nodeModulesPath = path.resolve(__dirname, 'node_modules');
const TransferWebpackPlugin = require('transfer-webpack-plugin');

const config = {
    entry: [path.join(__dirname, '/src/app/app.js')],
    // Render source-map file for final build
    devtool: 'source-map',
    // output config
    output: {
        path: buildPath, // Path of output file
        filename: 'app.js', // Name of output file
    },
    plugins: [
        // Minify the bundle
/*        new webpack.optimize.UglifyJsPlugin({
            compress: {
                // suppresses warnings, usually from module minification
                warnings: false
            }
        }),*/
        // Allows error warnings but does not stop compiling.
        new webpack.NoEmitOnErrorsPlugin(),
        // Transfer Files
        new TransferWebpackPlugin([
            {
                from: 'www'
            },
            {
                from: 'testApi',
                to: 'testApi'
            }
        ], path.resolve(__dirname, 'src'))
    ],
    module: {
        rules: [
            {
                test: /\.js$/, // All .js files
                loaders: ['babel-loader'], // react-hot is like browser sync and babel loads jsx and es6-7
                exclude: [nodeModulesPath]
            }
        ]
    },
    resolve: {
        modules: [
            // path.resolve('./src/app'),
            path.resolve('./lib'),
            "node_modules"
        ],
    }
};

module.exports = config;
