<?php
error_reporting(E_ALL & ~E_NOTICE);

header('Access-Control-Allow-Origin: *');
function makeid($len=10, $lettersOnly=false) {
	$text = "";
	$possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    $possibleLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    if($lettersOnly)
    	$possible = $possibleLetters;

	for($i=0; $i<$len; $i++) {
		if($i === 0)
			$text .= $possibleLetters[rand(0, strlen($possible))];
		else
			$text .= $possible[rand(0, strlen($possible))];
	}
	return $text;
}
function getFile($file, $default=null) {
	$result = $default;
	if(file_exists($file))
		$result = unserialize(file_get_contents($file));
	return $result;
}
function saveFile($file, $data) {
	file_put_contents($file, serialize($data));
}
$files = [
	'rooms' => $_SERVER["DOCUMENT_ROOT"].'/rooms.dat',
	'settings' => $_SERVER["DOCUMENT_ROOT"].'/settings.dat',
	'devices' => $_SERVER["DOCUMENT_ROOT"].'/devices.dat',
];
$rooms = getFile($files['rooms'], [
	[ "id" => "r1", "name" => "Гостиная" ],
	[ "id" => "r2", "name" => "Кухня" ],
	[ "id" => "r3", "name" => "Сортир" ]
]);
$devices = getFile($files['devices'], []);
$settings = getFile($files['settings'], [
    "phone" => [
        "name" => "Телефон",
        "value" => "999 999 99 99",
        "mask" => "999 999 99 99",
        "type" => "maskedText"
    ],
    "email" => [
        "name" => "Email",
        "value" => "putalexey@gmail.com"
    ],
    "show_advises" => [
        "name" => "Показывать советы",
        "value" => "1",
        "type" => "toggle"
    ]
]);

if (preg_match('/\.(?:png|jpg|jpeg|gif)$/', $_SERVER["REQUEST_URI"])) {
    return false;    // serve the requested resource as-is.
} else {
	$qpos = strpos($_SERVER["REQUEST_URI"], '?');
	if($qpos !== false)
    	$path = substr($_SERVER["REQUEST_URI"], 0, strpos($_SERVER["REQUEST_URI"], '?'));
    else $path = $_SERVER["REQUEST_URI"];
    header("Content-Type: application/javascript");
    switch($path) {
        case '/rooms-get':
            echo json_encode(array_values($rooms));
            break;
        case '/devices-get':
            echo json_encode($devices);
            break;
        case '/room-add':
            $id = makeid();
            $room = [
	            "id" => $id,
	            "name" => $_POST["name"],
            ];
            array_push($rooms, $room);
        	echo json_encode($room);
            break;
        case '/room-update':
        	$id = $_POST["id"];
        	foreach($rooms as &$room) {
        		if($room['id'] == $id) {
                    $room['name'] = $_POST["name"];
                    echo json_encode($room);
                }
        	}
            break;
            break;
        case '/room-delete':
        	$id = $_POST["id"];
			$rooms = array_filter($rooms, function($room) use ($id) {
				return $room['id'] != $id;
			});

			echo json_encode(['id' => $id]);
            break;
        case '/settings-get':
	        echo json_encode($settings);
            break;
        case '/settings-set':
            if(!empty($_POST["key"])) {
                $key = $_POST["key"];
            	$settings[$key]['value'] = $_POST["value"];
            }
        	break;
    }
}
saveFile($files['rooms'], $rooms);
saveFile($files['settings'], $settings);
saveFile($files['devices'], $devices);
