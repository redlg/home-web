<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>H.O.M.E</title>
    <meta name="description" content="Панель управления H.O.M.E">

    <!-- Use minimum-scale=1 to enable GPU rasterization -->
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0, maximum-scale=1, minimum-scale=1">
    <link rel="stylesheet" type="text/css" href="main.css">
    <link rel="stylesheet" type="text/css" href="fonts/fonts.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>

<body>
    <div id="app"></div>

    <!-- This script adds the Roboto font to our project. For more detail go to this site:  http://www.google.com/fonts#UsePlace:use/Collection:Roboto:400,300,500 -->
    <script>
    
        // var WebFontConfig = {
        //     google: {
        //         families: ['Roboto:400,300,500:latin,cyrillic']
        //     }
        // };
        // (function() {
        //     var wf = document.createElement('script');
        //     wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
        //     wf.type = 'text/javascript';
        //     wf.async = 'true';
        //     var s = document.getElementsByTagName('script')[0];
        //     s.parentNode.insertBefore(wf, s);
        // })();

        // window.addEventListener("beforeunload", function (e) {
        //       var confirmationMessage = "точно уйти со страницы?";
        //       e.returnValue = confirmationMessage;     // Gecko, Trident, Chrome 34+
        //       return confirmationMessage;              // Gecko, WebKit, Chrome <34
        //     });
    </script>
    <script src="jquery.min.js"></script>
    <script src="app.js?<?=filemtime(__DIR__ . '/app.js');?>"></script>
</body>

</html>
