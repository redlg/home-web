import Immutable from "immutable";
import DI from "../DI";
import HomeStore from "../HomeStore";
import AppDispatcher from "../AppDispatcher";
import SettingsActionCreators from "../Actions/SettingsActionCreators";

class SettingsStore extends HomeStore {
    constructor(dispatcher) {
        super(dispatcher);
        this.settings = Immutable.Map();
        DI.get('backend-socket').on('options.deleted', SettingsActionCreators.settingDeleted);
        DI.get('backend-socket').on('options.updated', SettingsActionCreators.settingUpdated);
        DI.get('backend-socket').on('options.set', SettingsActionCreators.valueUpdated);
    }
    __onDispatch(action) {
        switch (action.type) {
            case "settings/load":
                if(!this.loaded) {
                    DI.get('backend').sendRequest(
                        'options-get',
                        (settings) => {
                            AppDispatcher.dispatch({
                                type: "settings/loaded",
                                data: settings
                            });
                        }
                    );
                }
                break;
            case "settings/update-setting":
                DI.get('backend').sendRequest(
                    'option-update',
                    {
                        key: action.data.key,
                        params: action.data.params,
                    });
                // this.settings = this.settings.updateIn([action.data.key, 'value'], () => action.data.value);
                // this.__emitChange();
                break;
            case "settings/set-value":
                DI.get('backend').sendRequest(
                    'option-set',
                    {
                        key: action.data.key,
                        value: action.data.value,
                    });
                // this.settings = this.settings.updateIn([action.data.key, 'value'], () => action.data.value);
                // this.__emitChange();
                break;
            case "settings/delete-setting":
                DI.get('backend').sendRequest(
                    'option-delete',
                    {
                        key: action.data.key,
                        params: action.data.params,
                    });
                break;


            case "settings/updated-value":
                this.settings = this.settings.updateIn([action.data.key, 'value'], () => action.data.params.value);
                this.__emitChange();
                break;
            case "settings/updated-setting":
                if(action.data.option.params)
                    action.data.option.params = JSON.parse(action.data.option.params);
                if(!action.data.option.key)
                    action.data.option.key = action.data.key;

                if(this.settings.has(action.data.key))
                    this.settings = this.settings.mergeIn([action.data.key], action.data.option);
                else
                    this.settings = this.settings.set(action.data.key, Immutable.Map(action.data.option));
                this.__emitChange();
                break;
            case "settings/deleted-setting":
                this.settings = this.settings.delete(action.data.key);
                this.__emitChange();
                break;
            case "settings/loaded":
                this.loaded = true;
                let data = {};
                for(let i in action.data) {
                    if(action.data[i].params) {
                        try {
                            action.data[i].params = JSON.parse(action.data[i].params);
                        } catch (ex) {
                            console.warn(ex.message);
                            action.data[i].params = {};
                        }
                    }
                    data[action.data[i].key] = action.data[i];
                }
                if(action.data) {
                    this.settings = Immutable.fromJS(data);
                }
                this.__emitChange();
                break;
            default:

        }
    }
}

export default new SettingsStore(AppDispatcher);
