import HomeStore from "../HomeStore";
import AppDispatcher from "../AppDispatcher";
import Immutable from "immutable";
import DI from "../DI";

class RemotesStore extends HomeStore {
    constructor(dispatcher) {
        super(dispatcher);
        this.remotes = Immutable.Map();
    }
    __onDispatch(action) {
        // let backend = DI.get('backend-ajax');
        let backend = DI.get('backend');
        switch (action.type) {
            case "remotes/load":
                if(!this.loaded) {
                    backend.sendRequest(
                        'remotes-get',
                        (remotes) => {
                            AppDispatcher.dispatch({
                                type: "remotes/loaded",
                                data: { remotes }
                            });
                        }
                    );
                }
                break;
            case "remotes/reload":
                setTimeout(() => {
                    this.loaded = false;
                    AppDispatcher.dispatch({ type: "remotes/load", });
                }, 1);
                break;
            case "remotes/reload-keys":
                backend.sendRequest(
                    'remotes-get-keys',
                    {
                        remote: action.data.name
                    },
                    (keys) => {
                        AppDispatcher.dispatch({
                            type: "remotes/keys-loaded",
                            data: {
                                remote: action.data.name,
                                keys
                            }
                        });
                    }
                );
                break;
            case "remotes/loaded":
                this.loaded = true;
                this.remotes = Immutable.fromJS(action.data.remotes);
                this.__emitChange();
                break;
            case "remotes/keys-loaded":
                this.remotes = this.remotes.setIn([action.data.remote, 'keys'], Immutable.fromJS(action.data.keys));
                this.__emitChange();
                break;
        }
    }
}

var rs = new RemotesStore(AppDispatcher);
export default rs;
