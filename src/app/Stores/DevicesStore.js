import HomeStore from "../HomeStore";
import AppDispatcher from "../AppDispatcher";
import Immutable from "immutable";
import DI from "../DI";
import DevicesActionCreators from "../Actions/DevicesActionCreators";
import itemtypes from "../types";

class DevicesStore extends HomeStore {
    constructor(dispatcher) {
        super(dispatcher);
        this.devices = Immutable.List();
        DI.get('backend-socket').on('device.added', DevicesActionCreators.deviceAdded);
        DI.get('backend-socket').on('device.updated', DevicesActionCreators.deviceUpdated);
        DI.get('backend-socket').on('device.deleted', DevicesActionCreators.deviceDeleted);
        DI.get('backend-socket').on('guard.timer', DevicesActionCreators.guardTimer);
    }
    __onDispatch(action) {
        switch (action.type) {
            case "devices/load":
                if (!this.loaded) {
                    DI.get('backend').sendRequest('device-get', {a:1}, (devRepo) => {
                        DevicesActionCreators.devicesLoaded(devRepo);
                    });
                }
                break;
            case "devices/loaded":
                this.loaded = true;
                this.devices = Immutable.List(action.data.map(this.importDevice));
                console.log(this.devices);
                this.__emitChange();
                break;
            case "devices/find":
                DI.get('backend').sendRequest('devices-find');
                break;
            case "device/add":
                {
                    DI.get('backend').sendRequest('device-add', {
                        device: action.data.device,
                    });
                    break;
                }
            case "device/update":
                {
                    // if(action.data.changedValues.value && typeof action.data.changedValues.value !== 'string') {
                    //     action.data.changedValues.value = action.data.changedValues.value._export();
                    // }
                    // if(action.data.changedValues.last_values && typeof action.data.changedValues.last_values !== 'string') {
                    //     action.data.changedValues.last_values = action.data.changedValues.last_values._export();
                    // }
                    DI.get('backend').sendRequest('device-update', {
                        id: action.data.devid,
                        changedValues: action.data.changedValues,
                    });
                    break;
                }
            case "device/delete":
                {
                    // if(action.data.changedValues.value && typeof action.data.changedValues.value !== 'string') {
                    //     action.data.changedValues.value = action.data.changedValues.value._export();
                    // }
                    // if(action.data.changedValues.last_values && typeof action.data.changedValues.last_values !== 'string') {
                    //     action.data.changedValues.last_values = action.data.changedValues.last_values._export();
                    // }
                    DI.get('backend').sendRequest('device-delete', {
                        id: action.data.devid
                    });
                    break;
                }
            case "device/clear-room":
            case "device/set-room":
                {
                    DI.get('backend').sendRequest('device-update', {
                        id: action.data.devid,
                        changedValues: {
                            room_id: action.data.room_id
                        },
                    });
                    break;
                }
            case "device/added":
            {
                this.devices = this.devices.push(this.importDevice(action.data));
                this.__emitChange();
                break;
            }
            case "device/updated":
            {
                let devid = action.data.devid;
                let changedValues = action.data.changedValues;

                this.devices.forEach((dev) => {
                    if(dev.id == devid)
                        dev.setValues(changedValues);
                });

                this.__emitChange();
                break;
            }
            case "device/deleted":
            {
                let devid = action.data.devid;

                this.devices = this.devices.filter((dev) => {
                    return !(dev.id == devid);
                });

                this.__emitChange();
                break;
            }
            case "device/guard_timer":
            {
                let guard = this.getGuard();
                guard.timer = guard.timer || {};
                guard.timer.state = action.data.state;
                guard.timer.time_left = action.data.time_left;
                guard.timer.time_passed = action.data.time_passed;
                this.__emitChange();
                break;
            }
            default:

        }
    }

    importDevice(data) {
        let deviceClass = itemtypes.generateType(data._type);

        deviceClass.setValues(data);
        return deviceClass;
    }

    getDevicesByRoom(room_id) {
        return this.devices.filter((device) => device.room_id == room_id);
    }
    getDevicesByID(device_id) {
        return this.devices.find((device) => device.id == device_id);
    }
    getGuard() {
        return this.devices.find((device) => device.code == "virt_guard");
    }
    getAlarm() {
        return this.devices.find((device) => device.code == "virt_alarm");
    }
}

export default new DevicesStore(AppDispatcher);
