import HomeStore from "../HomeStore";
import AppDispatcher from "../AppDispatcher";
import Immutable from "immutable";
import DI from "../DI";
import NotificationsActionCreators from "../Actions/NotificationsActionCreators";


class NotificationsStore extends HomeStore {
    constructor(dispatcher) {
        super(dispatcher);
        this.notifications = Immutable.List();
        DI.get('backend-socket').on('notification.added', NotificationsActionCreators.emitAdded);
        DI.get('backend-socket').on('notification.updated', NotificationsActionCreators.emitUpdated);
    }
    __onDispatch(action) {
        switch (action.type) {
            case "notifications/load": {
                DI.get('backend-socket').sendRequest('notifications-get',
                    {
                        archive: action.data.archive,
                        page: action.data.page,
                    },
                    (notifications) => {
                        NotificationsActionCreators.emitLoaded(notifications);
                    });
                break;
            }
            case "notifications/loaded": {
                action.data.forEach((newNotification) => {
                    let key = this.notifications.findKey(value => value == newNotification.id);
                    if (key !== undefined) {
                        this.notifications = this.notifications.set(key, newNotification);
                    } else {
                        this.notifications = this.notifications.push(newNotification);
                    }
                });
                this.__emitChange();
                break;
            }
            case "notification/add": {
                DI.get('backend-socket').sendRequest('notification-add', {notification: action.data.values});
                break;
            }
            case "notification/update": {
                DI.get('backend-socket').sendRequest('notification-update', action.data); // data = {id, values}
                break;
            }
            case "notification/action": {
                DI.get('backend-socket').sendRequest('notification-action', action.data); // {id, action, action_params}
                break;
            }
            case "notification/added": {
                this.notifications = this.notifications.push(action.data);
                this.__emitChange();
                break;
            }
            case "notification/updated": {
                let key = this.notifications.findKey(value => value.id == action.data.notification.id);
                if (key !== undefined) {
                    this.notifications = this.notifications.set(key, action.data.notification);
                    this.__emitChange();
                }
                break;
            }
            default:
        }
    }
}

export default new NotificationsStore(AppDispatcher);
