import HomeStore from "../HomeStore";
import AppDispatcher from "../AppDispatcher";
import Immutable from "immutable";
import DI from "../DI";
import RoomsActionCreators from "../Actions/RoomsActionCreators";

class RoomsStore extends HomeStore {
    constructor(dispatcher) {
        super(dispatcher);
        this.rooms = Immutable.List();
        DI.get('backend-socket').on('rooms.added', RoomsActionCreators.roomAdded);
        DI.get('backend-socket').on('rooms.updated', RoomsActionCreators.roomUpdated);
        DI.get('backend-socket').on('rooms.deleted', RoomsActionCreators.roomDeleted);
    }
    __onDispatch(action) {
        // let backend = DI.get('backend-ajax');
        let backend = DI.get('backend');
        switch (action.type) {
            case "rooms/load":
                if(!this.loaded) {
                    backend.sendRequest(
                        'rooms-get',
                        (rooms) => {
                            AppDispatcher.dispatch({
                                type: "rooms/loaded",
                                data: { rooms }
                            });
                        }
                    );
                }
                break;
            case "rooms/room-add":
                backend.sendRequest(
                    'room-add',
                    {
                        name: action.data.newRoom.name
                    }
                );
                break;
            case "rooms/room-update":
                backend.sendRequest(
                    'room-update',
                    {
                        id: action.data.id,
                        name: action.data.newRoom.name
                    }
                );
                break;
            case "rooms/room-delete":
                backend.sendRequest(
                    'room-delete',
                    {
                        id: action.data.id
                    }
                );
                break;

            case "rooms/loaded":
                this.loaded = true;
                this.rooms = Immutable.fromJS(action.data.rooms);
                this.__emitChange();
                break;
            case "rooms/room-added":
                let room = this.get(action.data.id);
                if(!room) {
                    this.rooms = this.rooms.push(Immutable.fromJS(action.data));
                    this.__emitChange();
                } else {
                    AppDispatcher.dispatch({
                        type: 'rooms/room-updated',
                        data: {
                            id: room.get('id'),
                            changed: action.data,
                            room: action.data,
                        }
                    });
                }
                break;
            case "rooms/room-updated":
                this.rooms = this.rooms
                    .map(room => {
                        if(room.get('id') == action.data.id) {
                            return room.set('name', action.data.room.name);
                        }
                        return room;
                    });
                this.__emitChange();
                break;
            case "rooms/room-deleted":
                this.rooms = this.rooms.filter((room) => {
                    return room.get("id") != action.data.id;
                });
                this.__emitChange();
                break;
        }
    }
    get(roomId) {
        return this.rooms.find(function (room) {
            return room.get("id") == roomId;
        });
    }
}

var rs = new RoomsStore(AppDispatcher);
export default rs;
