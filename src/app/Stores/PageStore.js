import {Store} from "flux/utils";
import AppDispatcher from "../AppDispatcher";

class PageStore extends Store {
    constructor(dispatcher) {
        super(dispatcher);
        this.params = {
            title: ""
        };
    }
    __onDispatch = function(action) {
        switch(action.type) {
            case "page/set-title":
                this.params.title = action.data.title;
                this.__emitChange();
                break;

            case "page/set-value":
                this.set(action.data.name, action.data.value);
                this.__emitChange();
                break;
        }
    }
    get(name, def) {
        if((typeof this.params[name]) !== "undefined")
            return this.params[name];
        return def;
    }
    set(name, value) {
        this.params[name] = value;
    }
}

export default new PageStore(AppDispatcher);
