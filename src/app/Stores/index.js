import SettingsStore from "./SettingsStore";
import RoomsStore from "./RoomsStore";
import PageStore from "./PageStore";
import NotificationsStore from "./NotificationsStore";
import DevicesStore from "./DevicesStore";
import RemotesStore from "./RemotesStore";


(window || global).Stores = {
    DevicesStore,
    SettingsStore,
    RoomsStore,
    PageStore,
    NotificationsStore,
    RemotesStore
};
export {
    DevicesStore,
    SettingsStore,
    RoomsStore,
    PageStore,
    NotificationsStore,
    RemotesStore
};
