/**
 * Created by putal_000 on 22.08.2016.
 */
import React, {Component, PropTypes} from "react";
import IconButton from "material-ui/IconButton";
import IconActionSettings from "material-ui/svg-icons/action/settings";
import DeviceSettingsDialog from "./DeviceSettingsDialog";
import DevicesActionCreators from "../Actions/DevicesActionCreators"
import DeviceStore from "../Stores/DevicesStore"

export default class DeviceSettingsButton extends Component {
    static propTypes = {
        device_id: PropTypes.string.isRequired,
        iconStyles: PropTypes.object,
        style: PropTypes.object,
    };
    static contextTypes = {
        muiTheme: PropTypes.object.isRequired
    };
    constructor(props) {
        super(props);
        this.state = {
            dialogOpen: false,
            device: DeviceStore.getDevicesByID(props.device_id)
        }
    }
    handleClick = (event) => {
        this.setState({
            dialogOpen: true
        });
    };
    handleClose = () => {
        this.setState({
            dialogOpen: false
        });
    };
    handleDeviceDelete = () => {
        DevicesActionCreators.deviceDelete(this.props.device_id);
        this.setState({
            dialogOpen: false
        });
    };
    handleRoomSave = (newDevice) => {
        DevicesActionCreators.deviceUpdate(this.props.device_id, newDevice);
        this.setState({
            dialogOpen: false
        });
    };
    componentWillReceiveProps(newProps) {
        if(newProps.device_id != this.props.device_id) {
            this.setState({
                device: DeviceStore.getDevicesByID(newProps.device_id)
            });
        }
    }
    render() {
        const {muiTheme} = this.context;
        const {device_id, iconStyles, style} = this.props;
        const styles = Object.assign({}, muiTheme.cardSettingsStyles, style);
        let dialog;
        if(this.state.dialogOpen) {
            let dialog_class = this.state.device.constructor.dialog || DeviceSettingsDialog;
            dialog = React.createElement(dialog_class, {
                title: "Сменить имя устройства",
                modal: false,
                device_id: device_id,
                open: this.state.dialogOpen,
                onSave: this.handleRoomSave,
                onDelete: this.handleDeviceDelete,
                onRequestClose: this.handleClose,
                autoScrollBodyContent: true,
            });
            // dialog = <{dialog_class}
            //     title="Сменить имя устройства"
            //     modal={false}
            //     device_id={device_id}
            //     open={this.state.dialogOpen}
            //     onSave={this.handleRoomSave}
            //     onRequestClose={this.handleClose}/>
        }

        return <div className="device-setting-button">
            <IconButton onClick={this.handleClick} iconStyle={iconStyles} style={styles}>
                <IconActionSettings />
            </IconButton>
            {dialog}
        </div>;
    }
}
