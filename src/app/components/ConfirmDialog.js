import React, {Component, PropTypes} from 'react';
import { confirmable } from 'react-confirm';
import Dialog from "material-ui/Dialog";
import FlatButton from "material-ui/FlatButton";
import Theme from "../theme"

@confirmable
class ConfirmDialog extends Component {
    static propTypes = {
        show: PropTypes.bool,            // from confirmable. indicates if the dialog is shown or not.
        proceed: PropTypes.func,         // from confirmable. call to close the dialog with promise resolved.
        cancel: PropTypes.func,          // from confirmable. call to close the dialog with promise rejected.
        dismiss: PropTypes.func,         // from confirmable. call to only close the dialog.
        confirmation: PropTypes.string,  // arguments of your confirm function
        options: PropTypes.object        // arguments of your confirm function
    };

    constructor(props) {
        super(props);
    }

    render() {
        const {
            okLabel = 'OK',
            cancelLabel = 'Cancel',
            title,
            confirmation,
            show,
            proceed,
            dismiss,
            cancel,
            modal,
        } = this.props;

        const actions = [
            <FlatButton
                label={cancelLabel}
                secondary={true}
                onClick={cancel}
            />,
            <FlatButton
                label={okLabel}
                primary={true}
                keyboardFocused={true}
                onClick={proceed}
            />,
        ];

        return (
            <Theme>
                <Dialog
                    title={title}
                    actions={actions}
                    modal={modal}
                    open={show}
                    contentStyle={{
                        maxWidth: '300px'
                    }}
                    onRequestClose={dismiss}
                >
                    {confirmation}
                </Dialog>
            </Theme>
        );
    }
}

// confirmable HOC pass props `show`, `dismiss`, `cancel` and `proceed` to your component.
// export default confirmable(ConfirmDialog);
export default ConfirmDialog;