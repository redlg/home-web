import React, {Component, PropTypes} from "react";
import Dialog from "material-ui/Dialog";
import TextField from "material-ui/TextField";
import FlatButton from "material-ui/FlatButton";
import MenuItem from "material-ui/MenuItem";
import SelectField from "material-ui/SelectField";
import SettingsActionCreators from "../Actions/SettingsActionCreators";


export default class SettingsAddSettingDialog extends Component {
    static propTypes = {
        onHandleClose: React.PropTypes.func,
        open: React.PropTypes.bool.isRequired,
        title: React.PropTypes.string,
        // setting: React.PropTypes.object, // TODO реализовать редактирование
    };

    constructor(props) {
        super(props);

        this.state = {
            key: '',
            title: '',
            type: '',
            params: '',
        };
    }

    handleClose = (e) => {
        this.setState({
            key: '',
            title: '',
            type: '',
            params: '',
        });

        if (this.props.onHandleClose) {
            this.props.onHandleClose(e);
        }
    };

    handleSave = (e) => {
        SettingsActionCreators.updateSetting(
            this.state.key,
            {
                title: this.state.title,
                type: this.state.type,
                params: this.state.params,
            }
        );
        this.handleClose(e);
    };

    handleFieldChange = (e, value) => {
        let newState = {};
        newState[e.target.name] = e.target.value;
        this.setState(newState);
    };

    // handleSelectChange = (e, name, value) => {
    //     let newState = {};
    //     newState[name] = value;
    //     this.setState(newState);
    // };
    handleTypeSelectChange = (e, index, value) => {
        this.setState({
            type: value
        });
    };

    render() {
        const {title, open} = this.props;
        const addDialogActions = [
            <FlatButton
                label="Cancel"
                primary={true}
                onTouchTap={this.handleClose}
            />,
            <FlatButton
                label="Submit"
                primary={true}
                keyboardFocused={true}
                onTouchTap={this.handleSave}
            />,
        ];

        return (<Dialog
            title={title}
            actions={addDialogActions}
            open={open}
            onRequestClose={this.handleClose}
        >
            <TextField
                id="key"
                name="key"
                hintText="Идентификатор"
                value={this.state.key}
                onChange={this.handleFieldChange}
            />
            <br/>
            <TextField
                id="title"
                name="title"
                hintText="Название"
                value={this.state.title}
                onChange={this.handleFieldChange}
            />
            <br/>
            <SelectField
                floatingLabelText="Тип поля"
                id="type"
                name="type"
                value={this.state.type}
                onChange={this.handleTypeSelectChange} >
                <MenuItem value={null} primaryText="" />
                <MenuItem value="text" primaryText="Текст"/>
                <MenuItem value="maskedText" primaryText="Текст с маской ввода"/>
                <MenuItem value="checkbox" primaryText="Флажок"/>
                <MenuItem value="toggle" primaryText="Переключатель"/>
                <MenuItem value="button" primaryText="Кнопка"/>
            </SelectField>
            {/*<TextField
             id="type"
             name="type"
             hintText="Тип"
             value={this.state.type}
             onChange={this.handleFieldChange}
             />*/}
            <br/>
            <TextField
                id="params"
                name="params"
                hintText="Доп параметры (json объект)"
                value={this.state.params}
                onChange={this.handleFieldChange}
            />

        </Dialog>);
    }
}
