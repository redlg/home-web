import React, {Component, PropTypes} from "react";
import MaskedTextField from "../MaskedTextField";
import TextField from "material-ui/TextField";
import Toggle from "material-ui/Toggle";
import RaisedButton from "material-ui/RaisedButton";
import IconButton from "material-ui/IconButton";
import Checkbox from 'material-ui/Checkbox';
import SettingsStore from "../../Stores/SettingsStore";
import SettingsActionCreators from "../../Actions/SettingsActionCreators";
import {Tabs, Tab} from 'material-ui/Tabs';
import SettingsAddSettingDialog from '../SettingsAddSettingDialog';
import IconClose from "material-ui/svg-icons/navigation/close";
import confirm from '../../confirm'

class OptionsForm extends Component {
    settingsStoreToken = false;

    static propTypes() {
        return {settings: PropTypes.object.required, onFormSubmit: PropTypes.func};
    }
    static defaultProps = {
        settings: {},
    };
    static contextTypes = {
        muiTheme: PropTypes.object.isRequired,
    };
    constructor(props) {
        super(props);

        this.state = {
            settingPopupOpened: false,
            settings: SettingsStore.settings.toJS()
        };
    }
    handleChange = (event, newValue) => {
        let newSettings = Object.assign({}, this.state.settings);
        newSettings[event.target.name].value = newValue;
        this.setState({settings: newSettings});
    };
    handleClick = (event) => {
        let $btn = $(event.target).closest('button'),
            btn = $btn[0];

        SettingsActionCreators.setValue(btn.name, btn.value);
    };
    handleSubmit = (event) => {
        event.preventDefault();
        Object.keys(this.state.settings).forEach((settingKey) => {
            let setting = this.state.settings[settingKey];
            if(SettingsStore.settings.getIn([settingKey, 'value']) != setting.value) {
                SettingsActionCreators.setValue(settingKey, setting.value);
            }
        });
    };
    componentDidMount() {
        this.settingsStoreToken = SettingsStore.addListener(() => {
            this.setState({
                settings: SettingsStore.settings.toJS()
            });
        });
    }
    componentWillUnmount() {
        this.settingsStoreToken && this.settingsStoreToken.remove();
    }
    getFieldInput(setting, settingKey) {
        let field;
        switch (setting.type) {
            case "toggle":
                field = (<Toggle
                    name={settingKey}
                    label={setting.title}
                    toggled={setting.value == 1}
                    onToggle={this.handleChange}/>);
                break;
            case "checkbox":
                field = (<Checkbox
                    name={settingKey}
                    label={setting.title}
                    checked={setting.value == 1}
                    onCheck={this.handleChange}/>);
                break;
            case "maskedText":
                field = (<MaskedTextField
                    name={settingKey}
                    value={setting.value}
                    mask={setting.params.mask}
                    maskPlaceholder={setting.params.maskPlaceholder}
                    hintText={setting.title}
                    floatingLabelText={setting.title}
                    onChange={this.handleChange}/>);
                break;
            case "button":
                field = (<RaisedButton
                    name={settingKey}
                    value={(setting.params && setting.params.value) || 1}
                    label={setting.title}
                    onTouchTap={this.handleClick}/>);
                break;
            default:
                field = (<TextField
                    name={settingKey}
                    value={setting.value}
                    floatingLabelText={setting.title}
                    onChange={this.handleChange}/>);
        }
        return field;
    }
    static systemOptions = [
        'phone',
        'email',
        'sms_notification',
        'not_close_on_leakage',
        'turnoff_on_alarm',
        'change_aes_btn',
    ];
    render() {
        const styles = {
            block: {
                maxWidth: 350
            }
        };
        let appSettings = this.state.settings;

        let mainSettingsList = []; // Настройки по-умолчанию, которые нельзя удалять
        let settingsList = [];
        if (appSettings) {
            mainSettingsList = Object.keys(appSettings)
                .filter((settingKey) => (this.constructor.systemOptions.indexOf(settingKey) > -1))
                .map((settingKey) => {
                    let setting = appSettings[settingKey];
                    let field = this.getFieldInput(setting, settingKey);
                    return <div key={settingKey}>
                        {field}
                    </div>;
                });
            settingsList = Object.keys(appSettings)
                .filter((settingKey) => (this.constructor.systemOptions.indexOf(settingKey) === -1))
                .map((settingKey) => {
                    let setting = appSettings[settingKey];
                    let field = this.getFieldInput(setting, settingKey);
                    return <div key={settingKey}>
                        {field}
                        <IconButton onTouchTap={this.removeSetting.bind(this, setting)}>
                            <IconClose />
                        </IconButton><br/>
                    </div>;
                });
        }

        let formStyle = {
            // width: 256,
            // margin: "0 auto",
        };

        let settingsBox;
        if (settingsList.length) {
            settingsBox = <Tabs>
                <Tab label="Основные">
                    <div style={styles.block}>
                        {mainSettingsList}
                    </div>
                </Tab>
                <Tab label="Пользовательские настройки">
                    <div style={styles.block}>
                        {settingsList}
                    </div>
                </Tab>
            </Tabs>;
        } else {
            settingsBox = mainSettingsList;
        }

        return (
            <div style={formStyle}>
                <form onSubmit={this.handleSubmit}>
                    {settingsBox}
                    <RaisedButton label="Соханить" primary={true} style={{margin: 12}} type="submit"/>
                    <RaisedButton label="Добавить параметр" onTouchTap={this.openSettingEditPopup}/>
                </form>

                <SettingsAddSettingDialog
                    title="Добавить свойство"
                    onHandleClose={this.closeSettingEditPopup}
                    open={this.state.settingPopupOpened} />
            </div>
        );
    }

    openSettingEditPopup = () => {
        this.setState({
            settingPopupOpened: true
        });
    };
    closeSettingEditPopup = () => {
        this.setState({
            settingPopupOpened: false
        });
    };

    removeSetting = (setting, e) => {
        let message = "Уверены, что хотите удалить свойство"
            + (setting.title ? " " + setting.title : "")
            + "?";
        confirm(message).then(
            () => {
                // `proceed` callback
                SettingsActionCreators.delete(setting.key);
            }
        );
    }
}

export default OptionsForm;
