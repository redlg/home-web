import React, {Component} from "react";
import {List, ListItem} from "material-ui/List";
import TextField from 'material-ui/TextField';
import IconMenu from 'material-ui/IconMenu';
import RaisedButton from 'material-ui/RaisedButton';
import MenuItem from 'material-ui/MenuItem';
import IconButton from 'material-ui/IconButton';
import Paper from 'material-ui/Paper';
import Subheader from 'material-ui/Subheader';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import Draggable from 'react-draggable';
import {grey400, darkBlack, lightBlack} from 'material-ui/styles/colors';
import {Map} from 'immutable';

import RoomsStore from "../../Stores/RoomsStore";
import DevicesStore from "../../Stores/DevicesStore";
import RoomEditDialog from '../RoomEditDialog';
import RemoteEditDialog from '../RemoteEditDialog';
import SettingsDeviceItem from "../SettingsDeviceItem"
import SettingsRoomItem from "../SettingsRoomItem"
import SettingsDevicesList from "../SettingsDevicesList"
import RoomsActionCreators from "../../Actions/RoomsActionCreators";
import DevicesActionCreators from "../../Actions/DevicesActionCreators";

import types from "../../types"

export default class RoomsForm extends Component {
    constructor() {
        super();
        this.state = {
            rooms: RoomsStore.rooms,
            devices: DevicesStore.devices.filter(this.filterRemotes),
            draggingItemIndex: -1,
            moveItemToRoom: -1,
            editState: false,
            dialogOpen: false
        };
        this.roomsStoreToken = false;
        this.devicesStoreToken = false;
    }
    componentWillMount() {
        DevicesActionCreators.loadDevices();
        RoomsActionCreators.loadRooms();
    }
    componentDidMount() {
        this.roomsStoreToken = RoomsStore.addListener(() => {
            this.setState({rooms: RoomsStore.rooms});
        });
        this.devicesStoreToken = DevicesStore.addListener(() => {
            this.setState({devices: DevicesStore.devices.filter(this.filterRemotes)});
        });
    }
    componentWillUnmount() {
        this.roomsStoreToken && this.roomsStoreToken.remove();
        this.devicesStoreToken && this.devicesStoreToken.remove();
    }
    filterRemotes(device) {
        return device.code == 'virt_ir';
    }

    addRemoteHandle = (event) => {
        event.stopPropagation();
        this.setState({editState: 'new_remote', dialogOpen: true});
    };
    addRoomHandle = (event) => {
        event.stopPropagation();
        this.setState({editState: 'new_room', dialogOpen: true});
    };
    handleClose = () => {
        this.setState({editState: false});
    };
    handleRoomSave = (newRoom) => {
        if (newRoom.id) {
            RoomsActionCreators.updateRoom(newRoom.id, newRoom);
        } else {
            RoomsActionCreators.addRoom(newRoom);
        }
        this.setState({editState: false});
    };
    handleRemoteSave = (newRemote) => {
        console.log(newRemote);
        if (newRemote.id) {
            DevicesActionCreators.deviceUpdate(newRemote.id, newRemote);
        } else {
            DevicesActionCreators.deviceAdd(newRemote);
        }
        this.setState({editState: false});
    };
    render() {
        const rooms = this.state.rooms;

        // const devicesItems = [];

        const devicesItems = this.state.devices
            .filter(device => {
                let room = rooms.find((room) => room.get('id') == device.room_id);
                return  (!device.room_id || !room)
            })
            .map((device, index) => <SettingsDeviceItem key={device.id} device={device} />);
        const roomItems = rooms.map((room, roomIndex) => {
            const handleEditBtn = () => {
                this.setState({dialogOpen: true, editState: "edit_room", editRoom: room.get("id")});
            };
            const handleDeleteBtn = () => {
                RoomsActionCreators.deleteRoom(room.get("id"));
            };

            const iconButtonElement = (
                <IconButton touch={true}>
                    <MoreVertIcon color={grey400}/>
                </IconButton>
            );
            const rightIconMenuStyle = {
                cursor: "pointer"
            };
            const rightIconMenu = (
                <IconMenu touchTapCloseDelay={1} iconButtonElement={iconButtonElement}>
                    <MenuItem style={rightIconMenuStyle} onTouchTap={handleEditBtn}>Edit</MenuItem>
                    <MenuItem style={rightIconMenuStyle} onTouchTap={handleDeleteBtn}>Delete</MenuItem>
                </IconMenu>
            );

            let roomDragEnter = () => {
                if(this.state.draggingItemIndex >= 0) {
                    this.setState({
                        moveItemToRoom: roomIndex
                    });
                }
            };
            let roomDragLeave = () => {
                if(this.state.draggingItemIndex >= 0) {
                    this.setState({
                        moveItemToRoom: -1
                    });
                }
            };
            let children = this.state.devices
                .filter(device => device.room_id == room.get('id'))
                .map((device) => <SettingsDeviceItem key={device.id} device={device} />).toJS();
            /* if(this.state.moveItemToRoom == roomIndex) {
                let movingDevice = this.state.devices.get(this.state.draggingItemIndex);
                children.push(<ListItem
                    key={movingDevice.get("id")}
                    primaryText={movingDevice.get("code") + " " + movingDevice.get("value")} />);
            } */

            return <SettingsRoomItem
                key={room.get("id")}
                primaryText={room.get("name") || "Не названа"}
                room={room}
                nestedItems={children}
                onMouseOver={roomDragEnter}
                onMouseOut={roomDragLeave}
                initiallyOpen={true}
                rightIconButton={rightIconMenu}/>;
        });
        let dialog;
        if (this.state.editState == 'new_room' || this.state.editState == 'edit_room') {
            let dialogTitle = this.state.editState == 'new_room'
                ? "Новая комната"
                : "Изменить комнату";
                // open={this.state.dialogOpen}
            dialog = <RoomEditDialog
                open={true}
                title={dialogTitle}
                modal={false}
                room_id={this.state.editRoom}
                onSave={this.handleRoomSave}
                onRequestClose={this.handleClose}/>;
        }
        if (this.state.editState == 'new_remote') {
            let dialogTitle = "Новый пульт";
                // open={this.state.dialogOpen}
            dialog = <RemoteEditDialog
                open={true}
                title={dialogTitle}
                modal={false}
                remote_id={0}
                onSave={this.handleRemoteSave}
                onRequestClose={this.handleClose}/>;
        }

        return <div className="rooms-form">
            {dialog}
            <div className="rooms-form--side-left drag-panel">
                <Paper>
                    <SettingsDevicesList
                        style={{ paddingBottom: 0 }}>
                        <Subheader>Нераспределенные</Subheader>
                        {devicesItems.count() ? devicesItems : <ListItem primaryText="Нет пультов" disabled={true} />}
                        <RaisedButton label="Добавить пульт" fullWidth={true} onTouchTap={this.addRemoteHandle}/>
                    </SettingsDevicesList>
                </Paper>
            </div>
            <div className="rooms-form--side-right drag-panel">
                <Paper>
                    <List
                        style={{ paddingBottom: 0 }}>
                        {roomItems}
                        <RaisedButton label="Добавить комнату" fullWidth={true} onTouchTap={this.addRoomHandle}/>
                    </List>
                </Paper>
            </div>
        </div>;
    }
}
