/**
 * Created by putal_000 on 22.08.2016.
 */
import React, { PropTypes } from "react";
import clone from "clone";
import DevicesStore from "../Stores/DevicesStore";
import Dialog from 'material-ui/Dialog';
import TextField from 'material-ui/TextField';
import FlatButton from 'material-ui/FlatButton';

export default class DeviceSettingsDialog extends Dialog {
    static propTypes = {
        device_id: PropTypes.string.isRequired,
        onSave: PropTypes.func.isRequired,
    };

    componentWillMount() {
        let device = clone(DevicesStore.getDevicesByID(this.props.device_id));
        if(!device) {
            device = {};
        }
        this.state = {
            device
        };
    }
    componentWillReceiveProps(nextProps) {
        let device;//= DevicesStore.getDevicesByID(nextProps.device_id);
        if(nextProps.device_id && this.props.device_id != nextProps.device_id) {
            device = clone(DevicesStore.getDevicesByID(nextProps.device_id));
        }
        if(device ) {
            this.setState({ device });
        }
    }
    handleNameChange = (event) => {
        let device = this.state.device;
        device.name = event.target.value;
        this.setState({
            device: device
        });
    };
    // handleShowInRoomChange = (event) => {
    //     let device = this.state.device;
    //     console.log(event.target);
    //     device.settings.show_in_room = event.target.value;
    //     this.setState({
    //         device: device
    //     });
    // };
    handleFormSave = (event) => {
        event.preventDefault();
        this.handleSave(event);
    };
    handleSave = (event) => {
        event.stopPropagation();
        if(this.props.onSave) {
            this.props.onSave(this.state.device);
        }
    };
    handleClose = (event) => {
        event.stopPropagation();
        if(this.props.onRequestClose) {
            this.props.onRequestClose(true);
        }
    };

    render() {
        let { dialogActions, ...other } = this.props;

        if(!dialogActions) {
            dialogActions = [
                <FlatButton label="Cancel" primary={true}
                            onTouchTap={this.handleClose} />,
                <FlatButton label="Submit" type="submit" primary={true}
                            onTouchTap={this.handleSave} />
            ];
        }
        console.log(this.state.device);

        return <Dialog actions={dialogActions} {...other}>
            <form onSubmit={this.handleFormSave}>
                <TextField name="device_name"
                           floatingLabelText="Название устройства"
                           value={this.state.device.name || ''}
                           onChange={this.handleNameChange} />
            </form>
        </Dialog>;
    }
}
