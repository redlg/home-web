import React, {Component, PropTypes} from "react";
import {RoomsStore} from "../../Stores";
import DevicesStore from "../../Stores/DevicesStore";
import RoomsActionCreators from "../../Actions/RoomsActionCreators";
import DevicesActionCreators from "../../Actions/DevicesActionCreators";
import DeviceBase from "../Devices/Base";
import DeviceLed from "../Devices/Led";
import DeviceDimmer from "../Devices/Dimmer";
import DeviceColor from "../Devices/Color";
import DeviceSwitch from "../Devices/Switch";
import DeviceContact from "../Devices/Contact";
import DeviceLekg from "../Devices/Lekg";
import DeviceHumi from "../Devices/Humi";
import DeviceKettle from "../Devices/Kettle";
import DeviceRemote from "../Devices/Remote";
import Paper from "material-ui/Paper";
import PageTitle from "../../PageTitle";

class RoomPage extends Component {
    static propTypes = {
        params: PropTypes.object.isRequired,
    };

    constructor(props) {
        super(props);
        this.state = {
            room: RoomsStore.get(props.params.rid),
            devices: DevicesStore.getDevicesByRoom(props.params.rid)
        };
    }

    componentWillMount() {
        DevicesActionCreators.loadDevices();
        RoomsActionCreators.loadRooms();
    }
    componentDidMount() {
        this.roomsStoreToken = RoomsStore.addListener(() => {
            this.setState({
                room: RoomsStore.get(this.props.params.rid),
            });
        });
        this.devicesStoreToken = DevicesStore.addListener(() => {
            this.setState({
                devices: DevicesStore.getDevicesByRoom(this.props.params.rid)
            });
        });
    }
    componentWillUnmount() {
        this.roomsStoreToken && this.roomsStoreToken.remove();
        this.devicesStoreToken && this.devicesStoreToken.remove();
    }
    componentWillReceiveProps(nextProps) {
        this.setState({
            room: RoomsStore.get(nextProps.params.rid),
            devices: DevicesStore.getDevicesByRoom(nextProps.params.rid)
        });
    }

    render() {
        const {room, devices} = this.state;

        if(!room)
            return <h1>Комната не найдена</h1>;

        // const deviceList = devices.filter(device => !device.system).map((device) => {
        const deviceList = devices.map((device) => {
            let devType;
            let deviceStyle = {};
            if(!device.active) {
                deviceStyle["background"] = "rgb(222, 222, 222)";
            }
            switch (device._type) {
                case "Led":
                    devType = <DeviceLed style={deviceStyle} device={device} />;
                    break;
                case "Dimmer":
                    devType = <DeviceDimmer style={deviceStyle} device={device} />;
                    break;
                case "Color":
                    devType = <DeviceColor style={deviceStyle} device={device} />;
                    break;
                case "Switch":
                    devType = <DeviceSwitch style={deviceStyle} device={device} />;
                    break;
                case "Contact":
                    devType = <DeviceContact style={deviceStyle} device={device} />;
                    break;
                case "Lekg":
                    devType = <DeviceLekg style={deviceStyle} device={device} />;
                    break;
                case "IR":
                    break;
                case "VirtIR":
                    devType = <DeviceRemote style={deviceStyle} device={device} />;
                    break;
                case "Kettle":
                    devType = <DeviceKettle style={deviceStyle} device={device} />;
                    break;
                default:
                    if (device.code == "humi")
                        devType = <DeviceHumi style={deviceStyle} device={device} />;
                    else
                        devType = <DeviceBase style={deviceStyle} device={device} />;
            }
            if (devType) {
                return <div key={device.id} style={{margin: "24px auto"}}>{devType}</div>
            } else {
                return null;
            }
        });

        return (
            // <PageTitle title={room ? room.get("name") : ""}>
              <div>
                <h1>{room ? room.get('name') : ""}</h1>
                <div style={{ maxWidth: 400 }}>
                    {deviceList}
                </div>
              </div>
            // </PageTitle>
        );
    }
}

export default RoomPage;
