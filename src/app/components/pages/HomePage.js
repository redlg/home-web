import React, {Component, PropTypes} from "react";
import PageTitle from "../../PageTitle";

class HomePage extends Component {
    static contextTypes = {
        events: PropTypes.array,
        router: PropTypes.object.isRequired,
    }
    render() {
        console.log(this.context.router);
        console.log(this.props);
        let events = this.context.events.map(function(element, key) {
            return (<li key={key}>{element[0]} {element[1]}</li>);
        });

        return (<PageTitle title="Главная">
          <div>
            <h1>HomePage</h1>
            <h3>Events</h3>
            <ul>{events}</ul>
          </div>
        </PageTitle>);
    }
}

export default HomePage;
