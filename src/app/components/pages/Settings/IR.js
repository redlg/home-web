import React, {Component} from "react";
// import SettingsStore from "Stores/SettingsStore";
import IRDevicesForm from "../../Forms/IRDevicesForm";
import PageTitle from "../../../PageTitle";

class RoomsPage extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
          <PageTitle title="Настройка Пультов">
            <div>
                <h1>Пульты</h1>
                <IRDevicesForm />
            </div>
          </PageTitle>
        );
    }
}

export default RoomsPage;
