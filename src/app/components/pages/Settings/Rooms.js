import React, {Component} from "react";
// import SettingsStore from "Stores/SettingsStore";
import RoomsForm from "../../Forms/RoomsForm";
import PageTitle from "../../../PageTitle";

class Rooms extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
          <PageTitle title="Настройка комнат">
            <div>
                <h1>Комнаты</h1>
                <RoomsForm />
            </div>
          </PageTitle>
        );
    }
}

export default Rooms;
