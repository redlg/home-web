import React, {Component} from "react";
// import SettingsStore from "Stores/SettingsStore";
import OptionsForm from "../../Forms/OptionsForm";
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import SettingsActionCreators from '../../../Actions/SettingsActionCreators';
import PageTitle from "../../../PageTitle";

class Options extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        const styles = {
            marginLeft: 10
        };
        return (
            <PageTitle title="Настройки">
              <div>
                <h1>
                    Настройки
                </h1>
                <OptionsForm />
              </div>
            </PageTitle>
        );
    }
}

export default Options;
