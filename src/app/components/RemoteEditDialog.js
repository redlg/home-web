import Immutable from 'immutable';
import React, {Component, PropTypes} from "react";
import clone from "clone";
import Dialog from 'material-ui/Dialog';
import TextField from 'material-ui/TextField';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import FlatButton from 'material-ui/FlatButton';
import DevicesStore from "../Stores/DevicesStore";
import RemotesActions from "../Actions/RemotesActionCreators";
import RemotesStore from "../Stores/RemotesStore";
import RemoteKeysSettings from "../components/RemoteKeysSettings";
import VirtIR from "../types/VirtIR";

export default class RemoteEditDialog extends Dialog {
    constructor(props) {
        super(props);
        let remote = DevicesStore.getDevicesByID(props.device_id);
        if(!remote || remote.code != 'virt_ir') {
            remote = new VirtIR('', 'virt_ir');
            remote.name = 'Пульт';
        } else {
            remote = clone(remote);
        }
        console.log(remote);
        this.state = {
            remotes: RemotesStore.remotes,
            remote: remote
        };
    }
    static propTypes() {
        return {
            device_id: PropTypes.string,
            onSave: PropTypes.func.isRequired,
        };
    }

    componentWillMount() {
        RemotesActions.load();
    }
    componentDidMount() {
        this.remotesStoreToken = RemotesStore.addListener(() => {
            this.setState({remotes: RemotesStore.remotes});
        });
    }
    componentWillUnmount() {
        this.remotesStoreToken && this.remotesStoreToken.remove();
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.device_id && this.props.device_id != nextProps.device_id) {
            this.setState({
                remote: DevicesStore.getDevicesByID(nextProps.device_id)
            });
        }
    }
    handleNameChange = (event, value) => {
        let remote = this.state.remote;
        remote.name = value;

        this.setState({ remote });
    };
    handleTypeChange = (event, index, value) => {
        let remote = this.state.remote;
        remote.settings.device = value;
        remote.settings.keymap = [];

        this.setState({ remote });
    };
    handleFormSave = (event) => {
        event.preventDefault();
        this.handleSave(event);
    };
    handleSave = (event) => {
        event.stopPropagation();
        if(this.props.onSave) {
            this.props.onSave(this.state.remote);
        }
    };
    handleDelete = (event) => {
        event.stopPropagation();
        if(this.props.onDelete) {
            this.props.onDelete(true);
        }
    };
    handleClose = (event) => {
        event.stopPropagation();
        if(this.props.onRequestClose) {
            this.props.onRequestClose(true);
        }
    };
    handleKeymapChange = (value) => {
        let remote = this.state.remote;
        remote.settings.keymap = value;

        this.setState({ remote });
    };

    render() {
        let { dialogActions, ...other } = this.props;

        if(!dialogActions) {
            let submitLabel = this.state.remote.id ? 'Сохранить' : 'Создать';

            dialogActions = [];
            if (this.state.remote.id)
                dialogActions.push(<FlatButton label="Удалить" secondary={true}
                            onTouchTap={this.handleDelete} />);
            dialogActions.push(<FlatButton label="Отмена" primary={true}
                    onTouchTap={this.handleClose} />);
            dialogActions.push(<FlatButton label={submitLabel} type="submit" primary={true}
                    onTouchTap={this.handleSave} />);
        }
        let keysSettings = null;
        if(this.state.remote.settings.device) {
            keysSettings = <RemoteKeysSettings
                remotes={this.state.remotes}
                remote={this.state.remote}
                onKeymapChanged={this.handleKeymapChange}
                />
        }

        return <Dialog actions={dialogActions} {...other}>
            <form onSubmit={this.handleFormSave}>
                <TextField floatingLabelText="Название"
                           name="remote_name"
                           value={this.state.remote.name}
                           onChange={this.handleNameChange} />
                <br/>
                <SelectField floatingLabelText="Тип пульта"
                             name="remote_type"
                             value={this.state.remote.settings.device}
                             onChange={this.handleTypeChange}>
                    {this.state.remotes.map((remote) => {
                        return <MenuItem key={remote.get('name')}
                                         value={remote.get('name')}
                                         primaryText={remote.get('name')} />
                    }).toArray()}
                </SelectField>

                {keysSettings}

            </form>
        </Dialog>;
    }
}
