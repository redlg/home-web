import React, {Component, PropTypes} from "react";
import types from "../../types";
import Base from "./Base";
import { Card, CardHeader, CardText } from "material-ui/Card";
import Slider from "material-ui/Slider";
import Toggle from "material-ui/Toggle";
import RaisedButton from "material-ui/RaisedButton";
import { Tab, Tabs } from "material-ui/Tabs";
import DevicesActionCreators from "../../Actions/DevicesActionCreators";
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import DeviceSettingsButton from '../DeviceSettingsButton';
import clone from 'clone';
import {plural} from '../../utils.js';


export default class Led extends Base {
    static propTypes = {
        device: PropTypes.instanceOf(types.ItemType).isRequired,
        availableTypes: PropTypes.arrayOf(PropTypes.oneOf([ 'percent', 'rgb', 'garland', 'music' ]))
    };
    static defaultProps = {
        availableTypes: [
            'percent',
            'rgb',
            'garland',
            'music',
        ]
    };

    constructor(props, context) {
        super(props, context);
        this.sliderThemes = {
            red: getMuiTheme(this.context.muiTheme,
                {
                    slider: {
                        selectionColor: 'red',
                        handleColorZero: 'red',
                    }
                }),
            green: getMuiTheme(this.context.muiTheme,
                {
                    slider: {
                        selectionColor: 'green',
                        handleColorZero: 'green',
                    }
                }),
            blue: getMuiTheme(this.context.muiTheme,
                {
                    slider: {
                        selectionColor: 'blue',
                        handleColorZero: 'blue',
                    }
                }),
        };
    }

    handleLedSlider = (event, sliderValue) => {
        let value = clone(this.props.device.value);
        value[1] = sliderValue;

        DevicesActionCreators.deviceUpdate(this.props.device.id, {
            value: value
        });
    };
    handleRSlider = (event, sliderValue) => this.handleRGBSlider(event, sliderValue, 'red');
    handleGSlider = (event, sliderValue) => this.handleRGBSlider(event, sliderValue, 'green');
    handleBSlider = (event, sliderValue) => this.handleRGBSlider(event, sliderValue, 'blue');

    handleRGBSlider = (event, sliderValue, sliderName) => {
        let value = clone(this.props.device.value);
        if(!Array.isArray(value) || value.length < 4) {
            value = [2, 255, 127, 127];
        }
        switch (sliderName) {
            case 'red': value[1] = sliderValue; break;
            case 'green': value[2] = sliderValue; break;
            case 'blue': value[3] = sliderValue; break;
        }
        DevicesActionCreators.deviceUpdate(this.props.device.id, {
            value: value
        });
        // let value = this.props.device.value;
        // value.setValue(sliderValue);
        //
        // DevicesActionCreators.deviceUpdate(this.props.device.id, {
        //     value: value
        // });
        console.log(sliderValue, sliderName);
    };
    handleChangePattern = (event) => {
        DevicesActionCreators.deviceUpdate(this.props.device.id, {
            value: this.props.device.value
        });
    };
    toggleHandle = (event, newValue) => {
        let value = clone(this.props.device.value);
        let prevValue = clone(value);
        if(newValue) {
            value = clone(this.props.device.last_values);
            if(!Array.isArray(value)) value = [1, 255];
        } else {
            value = [0];
        }

        DevicesActionCreators.deviceUpdate(this.props.device.id, {
            value: value,
            last_values: prevValue,
        });
    };

    onTabChange = (value, event, tab) => {
        let prevValue = clone(this.props.device.value);
        switch(value) {
            case 'rgb':
                DevicesActionCreators.deviceUpdate(this.props.device.id, {
                    value: [2, 255, 127, 127],
                    last_values: prevValue,
                });
                break;
            case 'garland':
                DevicesActionCreators.deviceUpdate(this.props.device.id, {
                    value: [3, 1],
                    last_values: prevValue,
                });
                break;
            case 'music':
                DevicesActionCreators.deviceUpdate(this.props.device.id, {
                    value: [4],
                    last_values: prevValue,
                });
                break;
            default:
                DevicesActionCreators.deviceUpdate(this.props.device.id, {
                    value: [1, 255],
                    last_values: prevValue,
                });
                break;
        }
    };
    static contextTypes = {
        muiTheme: PropTypes.object.isRequired
    };
    render() {
        const {style, device, availableTypes} = this.props;
        const value = Array.isArray(device.value) ? device.value : [0];
        const { muiTheme } = this.context;
        const styles = {
            slider: {
                marginBottom: 0
            }
        };

        // let PercentController, ColorController, GarlandController, MusicController;
        let DeviceController;
        let turnedOn = false;
        let activeType = '';
        if(value[0] == 1 && availableTypes.indexOf('percent') >= 0) {
            activeType = 'percent';
            if (value[1] != 0) {
                turnedOn = true;
                DeviceController = <Slider
                    sliderStyle={styles.slider}
                    value={parseInt(value[1])}
                    min={1} max={255} step={1}
                    onChange={this.handleLedSlider}/>;
            }
        } else if(value[0] == 2 && availableTypes.indexOf('rgb') >= 0) {
            turnedOn = true;
            DeviceController = <div>
                <MuiThemeProvider muiTheme={this.sliderThemes.red}>
                    <Slider
                        sliderStyle={styles.slider}
                        name="red"
                        value={parseInt(value[1])}
                        min={0} max={255} step={1}
                        onChange={this.handleRSlider}/>
                </MuiThemeProvider>
                <MuiThemeProvider muiTheme={this.sliderThemes.green}>
                    <Slider
                        sliderStyle={styles.slider}
                        name="green"
                        value={parseInt(value[2])}
                        min={0} max={255} step={1}
                        onChange={this.handleGSlider}/>
                </MuiThemeProvider>
                <MuiThemeProvider muiTheme={this.sliderThemes.blue}>
                    <Slider
                        sliderStyle={styles.slider}
                        name="blue"
                        value={parseInt(value[3])}
                        min={0} max={255} step={1}
                        onChange={this.handleBSlider}/>
                </MuiThemeProvider>
            </div>;
            activeType = 'rgb';
        } else if(value[0] == 3 && availableTypes.indexOf('garland') >= 0) {
            turnedOn = true;
            let minutes = parseFloat(value[1]);
            let autoChangeLabel;
            if (minutes > 0) {
                let minutesLabel = plural(minutes, [
                    'каждую минуту',
                    'каждые {count} минуты',
                    'каждые {count} минут',
                    'каждую {count} минуту',
                ]);
                autoChangeLabel = <span>Автосмена рисунка {minutesLabel}</span>;
            } else {
                autoChangeLabel = <span>Автосмена рисунка отключена</span>;
            }
            DeviceController = <div>
                <Slider
                    sliderStyle={styles.slider}
                    name="minutes"
                    value={parseFloat(value[1])}
                    min={0} max={255} step={1}
                    onChange={this.handleLedSlider}/>
                {autoChangeLabel}
                <RaisedButton label="Сменить ресунок" labelColor="#fff" backgroundColor="#3f51b5"
                              style={{marginTop: 20}} fullWidth={true} onClick={this.handleChangePattern}/>
            </div>;
            activeType = 'garland';
        } else if(value[0] == 4 && availableTypes.indexOf('music') >= 0) {
            turnedOn = true;
            // DeviceController = <div>MusicController</div>;
            DeviceController = null;
            activeType = 'music';
        }
        let cardContent;
        if(availableTypes.length > 1) {
            let tabs = [];
            if (availableTypes.indexOf('percent') >= 0) tabs.push(
                <Tab key="percent" label="Свет" value="percent">
                    {activeType == 'percent' ? DeviceController : ""}
                </Tab>
            );
            if (availableTypes.indexOf('rgb') >= 0) tabs.push(
                <Tab key="rgb" label="Цвет" value="rgb">
                    {activeType == 'rgb' ? DeviceController : ""}
                </Tab>
            );
            if (availableTypes.indexOf('garland') >= 0) tabs.push(
                <Tab key="garland" label="Гирлянда" value="garland">
                    {activeType == 'garland' ? DeviceController : ""}
                </Tab>
            );
            if (availableTypes.indexOf('music') >= 0) tabs.push(
                <Tab key="music" label="Музыка" value="music">
                    {activeType == 'music' ? DeviceController : ""}
                </Tab>
            );
            cardContent = <Tabs style={styles.tabs} value={activeType} onChange={this.onTabChange}>{tabs}</Tabs>;
        } else {
            cardContent = DeviceController;
        }

        return <Card style={style} expanded={turnedOn}>
            <CardHeader title={device.name}>
                <DeviceSettingsButton device_id={device.id}/>
                <Toggle
                    style={muiTheme.cardToggle}
                    toggled={turnedOn}
                    onToggle={this.toggleHandle}
                />
            </CardHeader>
            <CardText expandable={true}>
                {cardContent}
            </CardText>
        </Card>;
    }
}