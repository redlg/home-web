import React, {Component, PropTypes} from "react";
import types from "../../types";
import Base from "./Base";
import { Card, CardHeader, CardText } from "material-ui/Card";
import DevicesActionCreators from "../../Actions/DevicesActionCreators";
import RemotesActionCreators from "../../Actions/RemotesActionCreators";
import RemotesStore from "../../Stores/RemotesStore";
import DeviceSettingsButton from '../DeviceSettingsButton';
import RaisedButton from 'material-ui/RaisedButton'

import {
    fullWhite,
    orange600 as iconWarmBack,
    red600 as iconPowerBack,
    orange800 as iconWarmHover,
    red800 as iconPowerHover,
} from 'material-ui/styles/colors'
import clone from "clone";

export default class Remote extends Base {
    static propTypes = {
        device: PropTypes.instanceOf(types.ItemType).isRequired
    };

    handleButton = (event, key) => {
        event.preventDefault();
        console.log('handleButton', key.toJS());
        RemotesActionCreators.sendKey(this.props.device.id, key);
        // const currentValue = this.props.device.value;
        // let value = clone(currentValue);
        // if (currentValue[0] > 0) {
        //     value[0] = 0;
        // } else {
        //     value = [1, value[1] || 100];
        // }
        // DevicesActionCreators.deviceUpdate(this.props.device.id, {value, last_values: currentValue})
    };

    constructor(props) {
        super(props);
        this.state = {
            remotes: RemotesStore.remotes,
            value: props.device.value
        };
    }

    componentWillMount() {
        setTimeout(RemotesActionCreators.load, 0);
    }
    componentDidMount() {
        this.remotesStoreToken = RemotesStore.addListener(() => {
            this.setState({remotes: RemotesStore.remotes});
        });
    }
    componentWillUnmount() {
        this.remotesStoreToken && this.remotesStoreToken.remove();
    }

    render() {
        const {style, device} = this.props;
        const value = device.value != null ? device.value : [0];
        const { muiTheme } = this.context;
        const styles = {
            slider: {
                marginBottom: 0
            },
            tabs: { }
        };

        const remoteButtons = (device.settings.keymap || []).map((keyId) => {
            let remoteDeviceType = this.state.remotes.get(device.settings.device);
            if (!remoteDeviceType) {
                return <span key={keyId}>Для устройства выбран невернтый тип</span>;
            }
            let settingKey = remoteDeviceType.get('keys').find((key) => key.get('id') == keyId);
            if (settingKey) {
                return <RaisedButton key={settingKey.get('id')} label={settingKey.get('title')} onTouchTap={(event) => {
                    this.handleButton(event, settingKey);
                }}/>
            }
            else {
                return <RaisedButton key={keyId} label={"Кнопка не найдена "+ keyId} disabled={true} />;
            }
        });

        return <Card style={style} expanded={true}>
            <CardHeader title={device.name}>
                <DeviceSettingsButton device_id={device.id}/>
            </CardHeader>
            <CardText expandable={true}>
                {remoteButtons}
            </CardText>
        </Card>;
    }
}