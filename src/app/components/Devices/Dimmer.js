import React, {Component, PropTypes} from "react";
import types from "../../types";
import Base from "./Base";
import { Card, CardHeader, CardText } from "material-ui/Card";
import Slider from "material-ui/Slider";
import Toggle from "material-ui/Toggle";
import DevicesActionCreators from "../../Actions/DevicesActionCreators";
import DeviceSettingsButton from '../DeviceSettingsButton';
import clone from "clone";

export default class Dimmer extends Base {
    static propTypes = {
        device: PropTypes.instanceOf(types.ItemType).isRequired
    };
    handleLedSlider = (event, sliderValue) => {
        let value = clone(this.props.device.value);
        value[1] = sliderValue;

        DevicesActionCreators.deviceUpdate(this.props.device.id, {
            value: value
        });
    };
    toggleHandle = (event, newValue) => {
        let value = clone(this.props.device.value);
        let prevValue = clone(value);
        if(newValue) {
            let new_value = clone(this.props.device.last_values);
            if(new_value == null || !Array.isArray(new_value)) new_value = [value[0], 255];
            else if(!new_value[1]) new_value[1] = 255;

            value = new_value;
        } else {
            value[1] = 0;
        }

        DevicesActionCreators.deviceUpdate(this.props.device.id, {
            value: value,
            last_values: prevValue,
        });
    };

    render() {
        const {style, device, availableTypes} = this.props;
        const value = device.value != null ? device.value : [0];
        const { muiTheme } = this.context;
        const styles = {
            slider: {
                marginBottom: 0
            },
            tabs: { }
        };

        // let PercentController, ColorController, GarlandController, MusicController;
        let DeviceController;
        let turnedOn = !!value[1];
        let modeDimmer = value[0] != 0;
        if (modeDimmer && turnedOn) {
            DeviceController = <Slider
                sliderStyle={styles.slider}
                value={parseInt(value[1])}
                min={1} max={255} step={1}
                onChange={this.handleLedSlider}/>;
        }


        return <Card style={style} expanded={modeDimmer && turnedOn}>
            <CardHeader title={device.name}>
                <DeviceSettingsButton device_id={device.id}/>
                <Toggle
                    style={muiTheme.cardToggle}
                    toggled={turnedOn}
                    onToggle={this.toggleHandle}
                />
            </CardHeader>
            <CardText expandable={modeDimmer}>
                {DeviceController}
            </CardText>
        </Card>;
    }
}