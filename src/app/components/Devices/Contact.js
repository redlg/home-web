import React, {Component, PropTypes} from "react";
import types from "../../types";
import Base from "./Base";
import { Card, CardHeader, CardText } from "material-ui/Card";
import Toggle from "material-ui/Toggle";
import DevicesActionCreators from "../../Actions/DevicesActionCreators";
import DeviceSettingsButton from '../DeviceSettingsButton';
import clone from 'clone';


export default class Switch extends Base {
    static propTypes = {
        device: PropTypes.instanceOf(types.ItemType).isRequired
    };
    toggleHandle = (event, newValue) => {
        let value;
        let prevValue = clone(this.props.device.value);
        if(newValue) {
            value = 1;
        } else {
            value = 0;
        }

        DevicesActionCreators.deviceUpdate(this.props.device.id, {
            value: value,
            last_values: prevValue,
        });
    };

    render() {
        const {style, device} = this.props;
        const value = device.value;
        const { muiTheme } = this.context;

        let turnedOn = !!value;

        return <Card style={style} expanded={turnedOn}>
            <CardHeader title={device.name}>
                <DeviceSettingsButton device_id={device.id}/>
                <Toggle
                    style={muiTheme.cardToggle}
                    toggled={turnedOn}
                    onToggle={this.toggleHandle}
                />
            </CardHeader>
        </Card>;
    }
}