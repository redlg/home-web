import React, {Component, PropTypes} from "react";
import { Card, CardHeader, CardText } from "material-ui/Card";
import DeviceSettingsButton from '../DeviceSettingsButton';

export default class Base extends Component {
    static contextTypes = {
        muiTheme: PropTypes.object.isRequired
    };
    render() {
        const { muiTheme } = this.context;

        const {style, device} = this.props;
        const value = (
            typeof device.value == "string" ||
            typeof device.value == "number") ? device.value : JSON.stringify(device.value);
        return <Card style={style}>
            <CardHeader title={device.name}>
                <DeviceSettingsButton device_id={device.id}/>
                <div style={muiTheme.cardToggle}>
                    {value}
                </div>
            </CardHeader>
        </Card>;
    }
}