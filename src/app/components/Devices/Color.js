import React, {Component, PropTypes} from "react";
import types from "../../types";
import Base from "./Base";
import { Card, CardHeader, CardText } from "material-ui/Card";
import Slider from "material-ui/Slider";
import Toggle from "material-ui/Toggle";
import { Tab, Tabs } from "material-ui/Tabs";
import DevicesActionCreators from "../../Actions/DevicesActionCreators";
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import DeviceSettingsButton from '../DeviceSettingsButton';
import clone from 'clone';


export default class Color extends Base {
    static propTypes = {
        device: PropTypes.instanceOf(types.ItemType).isRequired,
    };
    handleRSlider = (event, sliderValue) => this.handleRGBSlider(event, sliderValue, 'red');
    handleGSlider = (event, sliderValue) => this.handleRGBSlider(event, sliderValue, 'green');
    handleBSlider = (event, sliderValue) => this.handleRGBSlider(event, sliderValue, 'blue');

    handleRGBSlider = (event, sliderValue, sliderName) => {
        let value = clone(this.props.device.value);
        if(!Array.isArray(value) || value.length < 3) {
            value = [255, 127, 127];
        }
        switch (sliderName) {
            case 'red': value[0] = sliderValue; break;
            case 'green': value[1] = sliderValue; break;
            case 'blue': value[2] = sliderValue; break;
        }
        DevicesActionCreators.deviceUpdate(this.props.device.id, {
            value: value
        });

    };
    toggleHandle = (event, newValue) => {
        let value = clone(this.props.device.value);
        let prevValue = clone(value);
        if(newValue) {
            value = this.props.device.last_values;
            if(!Array.isArray(value) || value.length < 3)
                value = [255, 127, 127];
        } else {
            value = [0];
        }

        DevicesActionCreators.deviceUpdate(this.props.device.id, {
            value: value,
            last_values: prevValue,
        });
    };

    static contextTypes = {
        muiTheme: PropTypes.object.isRequired
    };
    render() {
        const {style, device} = this.props;
        const value = device.value;
        const { muiTheme } = this.context;
        const styles = {
            slider: {
                marginBottom: 0
            },
            cardToggle: {
                display: "inline-block",
                width: "auto",
                padding: "14px 12px",
                position: "absolute",
                top: 0,
                bottom: 0,
                right: 4,
            }
        };

        // let PercentController, ColorController, GarlandController, MusicController;
        let DeviceController;
        let turnedOn = false;
        if(Array.isArray(value) && value.length == 3) {
            turnedOn = true;
            const sliderThemes = {
                red: getMuiTheme(this.context.muiTheme,
                    {
                        slider: {
                            selectionColor: 'red',
                            handleColorZero: 'red',
                        }
                    }),
                green: getMuiTheme(this.context.muiTheme,
                    {
                        slider: {
                            selectionColor: 'green',
                            handleColorZero: 'green',
                        }
                    }),
                blue: getMuiTheme(this.context.muiTheme,
                    {
                        slider: {
                            selectionColor: 'blue',
                            handleColorZero: 'blue',
                        }
                    }),
            };
            DeviceController = <div>
                <MuiThemeProvider muiTheme={sliderThemes.red}>
                    <Slider
                        sliderStyle={styles.slider}
                        name="red"
                        value={parseInt(value[0])}
                        min={0} max={255} step={1}
                        onChange={this.handleRSlider}/>
                </MuiThemeProvider>
                <MuiThemeProvider muiTheme={sliderThemes.green}>
                    <Slider
                        sliderStyle={styles.slider}
                        name="green"
                        value={parseInt(value[1])}
                        min={0} max={255} step={1}
                        onChange={this.handleGSlider}/>
                </MuiThemeProvider>
                <MuiThemeProvider muiTheme={sliderThemes.blue}>
                    <Slider
                        sliderStyle={styles.slider}
                        name="blue"
                        value={parseInt(value[2])}
                        min={0} max={255} step={1}
                        onChange={this.handleBSlider}/>
                </MuiThemeProvider>
            </div>;
        }

        return <Card style={style} expanded={turnedOn}>
            <CardHeader title={device.name}>
                <DeviceSettingsButton device_id={device.id}/>
                <Toggle
                    style={muiTheme.cardToggle}
                    toggled={turnedOn}
                    onToggle={this.toggleHandle}
                />
            </CardHeader>
            <CardText expandable={true}>
                {DeviceController}
            </CardText>
        </Card>;
    }
}