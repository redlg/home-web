import React, {Component, PropTypes} from "react";
import types from "../../types";
import Base from "./Base";
import { Card, CardHeader, CardText } from "material-ui/Card";
import Slider from "material-ui/Slider";
import Toggle from "material-ui/Toggle";
import DevicesActionCreators from "../../Actions/DevicesActionCreators";
import DeviceSettingsButton from '../DeviceSettingsButton';
import FlatButton from 'material-ui/FlatButton'
import RaisedButton from 'material-ui/RaisedButton'
import CupIcon from 'material-ui/svg-icons/maps/local-cafe'
import FireIcon from 'material-ui/svg-icons/social/whatshot'
import classnames from 'classnames'
import {
    fullWhite,
    orange600 as iconWarmBack,
    red600 as iconPowerBack,
    orange800 as iconWarmHover,
    red800 as iconPowerHover,
} from 'material-ui/styles/colors'
import clone from "clone";

class KettleTempButton extends Component {
    static propTypes = {
        attributes: PropTypes.object,
        onItemClick: PropTypes.func,
        clickParams: PropTypes.any
    };
    __onClick = (event) => {
        if (this.props.onItemClick) {
            this.props.onItemClick(event, this.props.clickParams);
        }
    };
    render() {
        return <button {...this.props.attributes} onClick={this.__onClick}>
            {this.props.children}
        </button>;
    }
}

export default class Kettle extends Base {
    static propTypes = {
        device: PropTypes.instanceOf(types.ItemType).isRequired
    };

    handlePowerButton = (event, newValue) => {
        console.log('handlePowerButton');
        const currentValue = this.props.device.value;
        let value = clone(currentValue);
        if (currentValue[0] > 0) {
            value[0] = 0;
        } else {
            value = [1, value[1] || 100];
        }
        DevicesActionCreators.deviceUpdate(this.props.device.id, {value, last_values: currentValue})
    };

    handleHeatButton = (event, newValue) => {
        console.log('handleHeatButton');
        const currentValue = this.props.device.value;
        let value = clone(currentValue);
        if (currentValue[0] == 2) {
            value[0] = 0;
        } else {
            value[0] = 2;
            value[1] = value[1] || 100;
        }
        DevicesActionCreators.deviceUpdate(this.props.device.id, {value, last_values: currentValue})
    };

    handleTempButton = (event, temp) => {
        console.log('handleTempButton', temp);
        const currentValue = this.props.device.value;
        let value = clone(currentValue);
        value[1] = temp;
        DevicesActionCreators.deviceUpdate(this.props.device.id, {value, last_values: currentValue})
    };
    constructor(props) {
        super(props);
        this.state = {
            value: props.device.value
        };
    }

    render() {
        const {style, device} = this.props;
        const value = device.value != null ? device.value : [0];
        const { muiTheme } = this.context;
        const styles = {
            slider: {
                marginBottom: 0
            },
            tabs: { }
        };

        // let PercentController, ColorController, GarlandController, MusicController;
        let DeviceController;
        let turnedOn = !!value[1];
        let modeDimmer = value[0] != 0;
        if (modeDimmer && turnedOn) {
            DeviceController = <Slider
                sliderStyle={styles.slider}
                value={parseInt(value[1])}
                min={1} max={255} step={1}
                onChange={this.handleLedSlider}/>;
        }


        const tempButtons = [60, 70, 80, 90, 100].map(temp => {
            return <KettleTempButton
                key={"temp"+temp}
                attributes={{className: classnames({
                    "kettle_btn": true,
                    "active": value[1] == temp
                })}}
                onItemClick={this.handleTempButton} clickParams={temp}>{temp}</KettleTempButton>
        });
        return <Card style={style} expanded={value[0] > 0}>
            <CardHeader title={device.name}>
                <DeviceSettingsButton device_id={device.id}/>
                <div className="kettle_btns" style={{ top: 4, right: 110, position: 'absolute' }}>
                    <button className={classnames({
                        "kettle_btn heat": true,
                        "active": value[0] == 2
                    })} onClick={this.handleHeatButton}>
                        <CupIcon color={fullWhite} />
                    </button>
                    <button className={classnames({
                        "kettle_btn power": true,
                        "active": value[0] > 0
                    })} onClick={this.handlePowerButton}>
                        <FireIcon color={fullWhite} />
                    </button>
                </div>
            </CardHeader>
            <CardText expandable={true}>
                <div className="kettle_btns kettle_btns__justified">
                    {tempButtons}
                </div>
            </CardText>
        </Card>;
    }
}