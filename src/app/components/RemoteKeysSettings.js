import React, { Component, Element, PropTypes } from 'react';
import clone from 'clone';
import { DragSource, DropTarget } from 'react-dnd';
import { Preview } from 'react-dnd-multi-backend';
import DragTypes from '../DragTypes';
import RenderInBody from '../RenderInBody';

/**
 * Specifies the drag source contract.
 * Only `beginDrag` function is required.
 */
const remoteDragSource = {
    beginDrag(props, monitor, component) {
        console.log(props);
        // Return the data describing the dragged item
        return {
            remoteKey: props.remoteKey,
            from: props.from
        };
    },
};

@DragSource(DragTypes.REMOTE_BUTTON, remoteDragSource, (connect, monitor) => ({
    connectDragSource: connect.dragSource(),
    isDragging: monitor.isDragging()
}))
class RemoteButton extends Component {
    render() {
        const {isDragging, connectDragSource, from, remoteKey, style, children, ...others} = this.props;
        (style || {}).opacity = isDragging ? 0.8 : 1;
        return connectDragSource(<div style={style} {...others}>{children}</div>);
    }
}

const remoteDropTarget = {
    drop(props, monitor) {
        const item = monitor.getItem();
        props.saveKey(item.remoteKey, props.from, item.from);
    },
};
@DropTarget(DragTypes.REMOTE_BUTTON, remoteDropTarget, (connect, monitor) => ({
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver()
}))
class RemoteButtonList extends Component {
    render() {
        const {
            connectDropTarget,
            isOver,
            children,
            saveKey,
            from,
            ...other
        } = this.props;
        const changedChildren = React.Children.map(children, (child) => {
            return React.cloneElement(child, {from: this.props.from});
        });
        return connectDropTarget(<div {...other}>
            {changedChildren}
            </div>);
    }
}
export default class RemoteKeysSettings extends Component {
    static propTypes() {
        return {
            onKeymapChanged: PropTypes.func,
            remote: PropTypes.any.required,
            remotes: PropTypes.any.required
        }
    };
    handleSaveKey = (key, listTo, buttonFrom) => {
        if (listTo == buttonFrom) return;

        let newKeymap = clone(this.props.remote.settings.keymap) || [];
        if (listTo == 'selected_buttons' && buttonFrom == 'available_buttons') {
            if (newKeymap.indexOf(key.get('id')) === -1)
                newKeymap.push(key.get('id'));
        } else if (listTo == 'available_buttons' && buttonFrom == 'selected_buttons') {
            newKeymap = newKeymap.filter((_key) => _key != key.get('id') );
        }
        if (this.props.onKeymapChanged) {
            this.props.onKeymapChanged(newKeymap);
        }
    };

    getPreviewBody(type, item, style) {
        Object.assign(style, {
            zIndex: 10000
        });
        return <div style={style}>
            {item.remoteKey.get('title')}
        </div>
    };
    generatePreview = (type, item, style) => {
        return <RenderInBody render={()=>this.getPreviewBody(type, item, style)} />;
    };

    render() {
        const remoteSettings = this.props.remotes.get(this.props.remote.settings.device);
        if (remoteSettings) {
            const availableKeys = remoteSettings.get('keys')
                .map((key) => {
                    return <RemoteButton
                        key={key.get('id')}
                        remoteKey={key}>{key.get('title')}</RemoteButton>
                }).toJS();
            const selectedKeys = (this.props.remote.settings.keymap || [])
                .map((keyId) => {
                    let keySettings = remoteSettings.get('keys').find((_key) => _key.get('id') == keyId);
                    if (keySettings)
                        return <RemoteButton
                            key={keySettings.get('id')}
                            remoteKey={keySettings}>{keySettings.get('title')}</RemoteButton>;
                    else
                        return <div key={keyId}>key {keyId} not found</div>;
                });
            return <div>
                <Preview generator={this.generatePreview}/>
                <div className="leftside drag-panel" style={{float: 'left', width: '50%'}}>
                    Все кнопки
                    <RemoteButtonList saveKey={this.handleSaveKey}
                                      from="available_buttons"
                                      style={{
                                          width: '100%',
                                          minHeight: 100,
                                          background: 'lightgrey'
                                      }}>
                        {availableKeys}
                    </RemoteButtonList>
                </div>
                <div className="rightSide drag-panel" style={{float: 'left', width: '50%'}}>
                    Выбранные
                    <RemoteButtonList saveKey={this.handleSaveKey}
                                      from="selected_buttons"
                                      style={{
                                          width: '100%',
                                          minHeight: 100,
                                          background: 'lightgrey'
                                      }}>
                        {selectedKeys}
                    </RemoteButtonList>
                </div>
            </div>
        } else {
            return <div>Remote settings not found</div>
        }
    }
}