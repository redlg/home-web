import Immutable from 'immutable';
import React, {Component, PropTypes} from "react";
import Dialog from 'material-ui/Dialog';
import TextField from 'material-ui/TextField';
import FlatButton from 'material-ui/FlatButton';
import RoomsStore from "../Stores/RoomsStore";
import RoomsActionCreators from "../Actions/RoomsActionCreators";

export default class RoomsEditDialog extends Dialog {
    constructor(props) {
        super(props);
        let room = RoomsStore.get(props.room_id);
        if(!room) {
            room = Immutable.Map();
        }
        this.state = {
            room
        };
    }
    static propTypes() {
        return {
            room_id: PropTypes.string,
            onSave: PropTypes.func.isRequired,
        };
    }

    componentWillReceiveProps(nextProps) {
        let room = RoomsStore.get(nextProps.room_id);
        if(nextProps.room_id && this.props.room_id != nextProps.room_id) {
            room = RoomsStore.get(nextProps.room_id);
        }
        if(room) {
            this.setState({ room });
        }
    }
    handleNameChange = (event) => {
        this.setState({
            room: this.state.room.set('name', event.target.value)
        });
    };
    handleFormSave = (event) => {
        event.preventDefault();
        this.handleSave(event);
    };
    handleSave = (event) => {
        event.stopPropagation();
        if(this.props.onSave) {
            this.props.onSave(this.state.room.toJS());
        }
    };
    handleClose = (event) => {
        event.stopPropagation();
        if(this.props.onRequestClose) {
            this.props.onRequestClose(true);
        }
    };

    render() {
        let { dialogActions, ...other } = this.props;

        if(!dialogActions) {
            dialogActions = [
                <FlatButton label="Cancel" primary={true}
                    onTouchTap={this.handleClose} />,
                <FlatButton label="Submit" type="submit" primary={true}
                    onTouchTap={this.handleSave} />
            ];
        }

        return <Dialog actions={dialogActions} {...other}>
            <form onSubmit={this.handleFormSave}>
                <TextField name="room_name" value={this.state.room.get('name', '')} onChange={this.handleNameChange} />
            </form>
        </Dialog>;
    }
}
