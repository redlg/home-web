import React, {Component, PropTypes} from "react";
import withWidth, {SMALL, MEDIUM, LARGE} from "material-ui/utils/withWidth";
import spacing from "material-ui/styles/spacing";
import SettingsActions from "../Actions/SettingsActionCreators";
import RoomsActions from "../Actions/RoomsActionCreators";

import ColorPaletteDemo from "./ColorPaletteDemo";

import AppBar from "material-ui/AppBar";
import {PageStore, DevicesStore} from "../Stores";
import AppNavDrawer from "./AppNavDrawer";
import AppNotifications from './AppNotifications';
import GuardDevice from './Devices/Guard';

@withWidth()
class Master extends Component {
    static contextTypes = {
        muiTheme: PropTypes.object.isRequired,
        router: PropTypes.object.isRequired
    };
    static propTypes = {
        width: PropTypes.oneOf([SMALL, MEDIUM, LARGE]),
        children: PropTypes.element,
        location: PropTypes.any
    };
    static childContextTypes = {
        events: PropTypes.array
    };
    constructor(props) {
        super(props);
        this.state = {
            title: "Главная",
            navDrawerOpen: false,
            guard: DevicesStore.getGuard(),
            alarm: DevicesStore.getAlarm(),
            events: []
        };
    }

    getChildContext() {
        return {events: this.state.events};
    }

    componentDidMount() {
        SettingsActions.loadSettings();
        RoomsActions.loadRooms();

        this._pageStoreToken = PageStore.addListener(() => {
            this.setState({
                title: PageStore.get('title')
            });
        });
        this._devicesStoreToken = DevicesStore.addListener(() => {
            this.setState({
                guard: DevicesStore.getGuard(),
                alarm: DevicesStore.getAlarm(),
            });
        });
    }
    componentWillUnmount() {
        this._pageStoreToken && this._pageStoreToken.remove();
        this._devicesStoreToken && this._devicesStoreToken.remove();
    }

    getStyles() {
        const styles = {
            root: {
                paddingTop: spacing.desktopKeylineIncrement,
                minHeight: 400
            },
            appBar: {
                position: "fixed",
                // Needed to overlap the examples
                zIndex: this.context.muiTheme.zIndex.appBar + 1,
                top: 0
            },
            content: {
                margin: spacing.desktopGutter
            },
            contentWhenMedium: {
                margin: `${spacing.desktopGutter * 2}px ${spacing.desktopGutter * 3}px`
            }
        };
        if (this.props.width === MEDIUM || this.props.width === LARGE) {
            styles.content = Object.assign(styles.content, styles.contentWhenMedium);
        }
        return styles;
    }
    handleTouchTapLeftIcon = () => {
        this.setState({
            navDrawerOpen: !this.state.navDrawerOpen
        });
    };
    handleChangeNavDrawerState = (open, reason) => {
        this.setState({
            navDrawerOpen: open,
            events: this.state.events.concat([
                [open, reason]
            ])
        });
    };
    handleChangeList = (event, url) => {
        this.context.router.push(url);
        this.setState({
            navDrawerOpen: false
        });
    };
    handleNotificationClick = (event) => {

    };
    render() {
        let title = this.state.title;
        let styles = this.getStyles();
        const {prepareStyles} = this.context.muiTheme;

        let docked = false;
        let opened = this.state.navDrawerOpen;
        let appBarShowMenuIconButton = true;
        if (this.props.width == MEDIUM || this.props.width == LARGE) {
            docked = true;
            opened = true;
            appBarShowMenuIconButton = false;
            styles.appBar.paddingLeft = 256 + spacing.desktopGutter;
            styles.root.paddingLeft = 256;
        }

        /*<MuiThemeProvider muiTheme={this.state.muiTheme}>*/
        let {guard, alarm} = this.state;
        let navbarRightElements = <div className="topbar-components">
            {guard && <GuardDevice className="topbar-components__item" device={guard} alarm={alarm} />}
            <AppNotifications className="topbar-components__item" />
        </div>;
        return (
            <div>
                <ColorPaletteDemo />
                <AppBar
                  style={styles.appBar}
                  showMenuIconButton={appBarShowMenuIconButton}
                  onLeftIconButtonTouchTap={this.handleTouchTapLeftIcon}
                  iconElementRight={navbarRightElements}
                  title={title}>
                </AppBar>
                <div style={prepareStyles(styles.root)}>
                    <div style={prepareStyles(styles.content)}>
                        {this.props.children}
                    </div>
                </div>
                <AppNavDrawer location={this.props.location} onChangeList={this.handleChangeList} onRequestChangeNavDrawer={this.handleChangeNavDrawerState} docked={docked} open={opened}/>
            </div>
        );
    }
}
export default Master;
