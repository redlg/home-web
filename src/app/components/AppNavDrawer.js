import React, {Component, PropTypes} from "react";
import Drawer from "material-ui/Drawer";
import {List, ListItem, makeSelectable} from "material-ui/List";
import {spacing, typography} from "material-ui/styles";
import {RoomsStore} from "../Stores";
import DevicesActionCreators from "../Actions/DevicesActionCreators";
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';

const SelectableList = makeSelectable(List);

class AppNavDrawer extends Component {
    roomsStoreToken = false;

    static propTypes = {
        docked: PropTypes.bool.isRequired,
        onRequestChangeNavDrawer: PropTypes.func.isRequired,
        onChangeList: PropTypes.func.isRequired,
        open: PropTypes.bool.isRequired,
        location: PropTypes.object.isRequired,
        style: PropTypes.object
    }
    static defaultProps = {
        docked: true,
        open: false
    };
    static contextTypes = {
        muiTheme: PropTypes.object.isRequired,
        router: PropTypes.object.isRequired
    };
    constructor(props) {
        super(props);
        this.state = {
            rooms: RoomsStore.rooms.toJS()
        };
    }
    handleTouchTapHeader = (event) => {
        event.preventDefault();
        this.props.onChangeList(event, "/");
    };
    handleRoomChange = () => {
        this.setState({rooms: RoomsStore.rooms.toJS()});
    };
    handleFindDevices = () => {
        DevicesActionCreators.findDevices();
    };
    componentDidMount() {
        this.roomsStoreToken = RoomsStore.addListener(this.handleRoomChange);
    }
    componentWillUnmount() {
        this.roomsStoreToken && this.roomsStoreToken.remove();
    }
    render() {
        const {location, onRequestChangeNavDrawer, onChangeList} = this.props;
        let muiTheme = this.context.muiTheme;

        const styles = {
            header: {
                fontSize: 24,
                backgroundColor: muiTheme.palette.primary1Color,
                paddingLeft: spacing.desktopGutter,
                marginBottom: 8
            },
            logo: {
                cursor: "pointer",
                userSelect: "none",
                color: muiTheme.palette.alternateTextColor,
                lineHeight: `${muiTheme.spacing.desktopKeylineIncrement}px`,
                fontWeight: typography.fontWeightLight,
            },
            findBtn: {
                float: "right",
                marginTop: 10,
                marginRight: 10,
            }
        };

        var roomList = this.state.rooms.map(function(room) {
            return <ListItem key={room.id} primaryText={room.name} value={"/room/" + room.id}/>;
        });
        var settingsList = [
            <ListItem primaryText="Комнаты" value="/settings/rooms" />,
            <ListItem primaryText="Пульты" value="/settings/ir" />,
        ];

        return (
            <Drawer onRequestChange={onRequestChangeNavDrawer} docked={this.props.docked} open={this.props.open}>
                <div style={styles.header}>
                    <span style={styles.logo} onTouchTap={this.handleTouchTapHeader}>H.O.M.E. 2.0</span>

                    <FloatingActionButton mini={true} style={styles.findBtn} backgroundColor="#19e619" onClick={this.handleFindDevices}>
                        <ContentAdd />
                    </FloatingActionButton>
                </div>
                <SelectableList value={location.pathname} onChange={onChangeList}>
                    <ListItem primaryText="Комнаты" value="/room" initiallyOpen={true} primaryTogglesNestedList={true} nestedItems={roomList}/>
                    <ListItem primaryText="Настройки" value="/settings" initiallyOpen={false} nestedItems={settingsList}/>
                </SelectableList>
            </Drawer>
        );
    }
}

export default AppNavDrawer;
