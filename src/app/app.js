/*eslint no-unused-vars: ["error", { "varsIgnorePattern": "Socket" }]*/
import DI from "./DI";

import React from "react";
import {render} from "react-dom";
import injectTapEventPlugin from "react-tap-event-plugin";
import {Router, hashHistory } from "react-router";
import AppRoutes from "./AppRoutes";
// import HTML5Backend from 'react-dnd-html5-backend';
import MultiBackend, { TouchTransition } from 'react-dnd-multi-backend'
import TouchBackend from 'react-dnd-touch-backend';
import HTML5toTouch from 'react-dnd-multi-backend/lib/HTML5toTouch';
import { DragDropContext } from 'react-dnd';
import PageTitle from "./PageTitle";
import Theme from "./theme";

let _router = DragDropContext(MultiBackend({
    backends: [
        {
            backend: TouchBackend({enableMouseEvents: true}), // Note that you can call your backends with options
            preview: true,
            transition: TouchTransition
        }
    ]
}))(Router);

// Needed for onTouchTap
// http://stackoverflow.com/a/34015469/988941
injectTapEventPlugin();

// Render the main app react component into the app div.
// For more details see: https://facebook.github.io/react/docs/top-level-api.html#react.render
setTimeout(() => {
render(
    <Theme>
        <PageTitle title="H.O.M.E">
            <_router history={hashHistory} onUpdate={() => window.scrollTo(0, 0)}>
            {AppRoutes}
            </_router>
        </PageTitle>
    </Theme>, document.getElementById("app"));
}, 1000);