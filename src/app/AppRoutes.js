import React from "react";
import {Route, IndexRoute} from "react-router";
import Master from "./components/Master";
import HomePage from "./components/pages/HomePage";
import RoomPage from "./components/pages/RoomPage";
import SettingsOptionsPage from "./components/pages/Settings/Options";
import SettingsRoomsPage from "./components/pages/Settings/Rooms";
import IRPage from "./components/pages/Settings/IR";

var AppRoutes = (
    <Route path="/" component={Master}>
        <IndexRoute component={HomePage}/>
        <Route path="home" component={HomePage}/>
        <Route path="room(/:rid)" component={RoomPage}>
            <IndexRoute component={HomePage}/>
        </Route>
        <Route path="settings" component={SettingsOptionsPage}/>
        <Route path="settings/rooms" component={SettingsRoomsPage}/>
        <Route path="settings/ir" component={IRPage}/>
    </Route>
);

export default AppRoutes;
