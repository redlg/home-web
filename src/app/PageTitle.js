'use strict';

var React = require('react'),
    withSideEffect = require('react-side-effect'),
    PageActionCreators = require('./Actions/PageActionCreators').default;

function reducePropsToState(propsList) {
    var innermostProps = propsList[propsList.length - 1];
    if (innermostProps) {
        return innermostProps.title;
    }
}

function handleStateChangeOnClient(title) {
    var nextTitle = title || '';
    if (nextTitle !== document.title) {
        document.title = nextTitle;
        PageActionCreators.setTitle(nextTitle);
    }
}

var PageTitle = React.createClass({
    displayName: 'PageTitle',

    propTypes: {
        title: React.PropTypes.string.isRequired,
        children: React.PropTypes.any
    },

    render: function render() {
        if (this.props.children) {
            return React.Children.only(this.props.children);
        } else {
            return null;
        }
    }
});

module.exports = withSideEffect(
    reducePropsToState,
    handleStateChangeOnClient
)(PageTitle);
