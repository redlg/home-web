/**
 * Created by User on 11.02.2017.
 */

/**
 * @param count
 * @param plurals array of phrase versions ['one', 'few', 'many', 'other']
 * @returns {*}
 */
function plural(count, plurals) {
    let tens = count % 100;
    let ones = tens % 10;

    plurals = plurals.map((item) => item.replace("{count}", count));
    if (plurals.length < 3) {
        plurals[2] = plurals[1];
    }
    if (plurals.length < 4) {
        plurals[3] = plurals[0];
    }

    if (count == 1) {
        return plurals[0];
    } else if (ones > 1 && ones < 5) {
        return plurals[1];
    } else if (ones == 1 && !(tens > 10 && tens < 21)) {
        return plurals[3];
    } else {
        return plurals[2];
    }
}
export { plural };