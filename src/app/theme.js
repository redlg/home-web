import React, { Component, PropTypes } from 'react';

import lightBaseTheme from 'material-ui/styles/baseThemes/lightBaseTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';

// import injectTapEventPlugin from 'react-tap-event-plugin';
// injectTapEventPlugin();

class Theme extends Component {
    constructor(props) {
        super(props);
        this.state = {
            muiTheme: getMuiTheme({
                cardToggle: {
                    display: "inline-block",
                    width: "auto",
                    padding: 12,
                    position: "absolute",
                    top: 0,
                    bottom: 0,
                    right: 4,
                },
                cardSettingsStyles: {
                    top: 0,
                    right: 58,
                    position: "absolute"
                },
                guard: {
                    guardOffColor: "#fff",
                    guardTimerColor: "#fff",
                    guardOnColor: "#d9d571",
                    alarmedColor: "#d94a38",
                }
            })
        };
    }

    render() {
        return <MuiThemeProvider muiTheme={this.state.muiTheme}>
            { this.props.children }
        </MuiThemeProvider>;
    }
}

export default Theme;