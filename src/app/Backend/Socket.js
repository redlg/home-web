import io from "socket.io-client";
import config from "../config";

let socket;
let server = config.socketio_host;
socket = io(server);
socket.on('connect', function() {
    console.log('connected to ' + server);
});
socket.on('event', function(data) {
    console.log(data);
});
socket.on('reconnect', function(num) {
    console.log('reconnected after ' + num + ' attempts to ' + server);
});
socket.on('disconnect', function() {
    console.log('disconnected from ' + server);
});

export default socket;
