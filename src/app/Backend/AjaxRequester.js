import BaseRequester from "./BaseRequester";

export default class AjaxRequester extends BaseRequester {
    constructor() {
        super();
        this.endpoint = 'http://home.bugkiller.ru/testApi/index.php'
    }
    sendRequest(event, data, callback) {
        if(typeof data === "function") {
            callback = data;
            data = undefined;
        }
        return $.ajax({
            dataType: 'json',
            method: 'post',
            data: data,
            url: this.endpoint + event,
            success: function(data) {
                if(callback) {
                    callback(data);
                }
            }
        });
    }
};