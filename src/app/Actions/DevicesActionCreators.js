import AppDispatcher from "../AppDispatcher";

export default {
    loadDevices : function() {
        AppDispatcher.dispatch({type: "devices/load"});
    },

    findDevices : function() {
        AppDispatcher.dispatch({
            type: "devices/find"
        });
    },

    devicesLoaded : function(devices) {
        AppDispatcher.dispatch({type: "devices/loaded", data: devices});
    },

    deviceUpdate : function(devid, changedValues) {
        AppDispatcher.dispatch({
            type: "device/update",
            data: {
                devid,
                changedValues,
            }
        });
    },

    deviceAdd : function(device) {
        AppDispatcher.dispatch({
            type: "device/add",
            data: {
                device,
            }
        });
    },

    deviceDelete : function(devid) {
        AppDispatcher.dispatch({
            type: "device/delete",
            data: {
                devid,
            }
        });
    },

    deviceAdded : function(device) {
        AppDispatcher.dispatch({
            type: "device/added",
            data: device
        });
    },

    deviceUpdated : function(devid, changedValues, newDeviceState) {
        AppDispatcher.dispatch({
            type: "device/updated",
            data: {
                devid,
                changedValues,
                newDeviceState
            }
        });
    },
    deviceDeleted : function(devid) {
        AppDispatcher.dispatch({
            type: "device/deleted",
            data: {
                devid
            }
        });
    },

    setRoom: function(devid, room_id) {
        AppDispatcher.dispatch({
            type: "device/set-room",
            data: {
                devid,
                room_id,
            }
        });
    },

    clearRoom: function(devid) {
        AppDispatcher.dispatch({
            type: "device/clear-room",
            data: {
                devid,
                room_id: null
            }
        });
    },
    guardTimer : function(state, time_left, time_passed) {
        AppDispatcher.dispatch({
            type: "device/guard_timer",
            data: {
                state,
                time_left,
                time_passed
            }
        });
    },
};
