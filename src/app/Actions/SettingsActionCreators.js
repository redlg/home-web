import AppDispatcher from "../AppDispatcher";

export default {
    loadSettings: function() {
        AppDispatcher.dispatch({
            type: "settings/load"
        });
    },

    settingsLoaded: function(settings) {
        AppDispatcher.dispatch({
            type: "settings/loaded",
            data: settings
        });
    },
    // не помню, нужно ли...
    setParam: function(key, value) {
        AppDispatcher.dispatch({
            type: "settings/set-value",
            data: {
                key,
                value
            }
        });
    },
    setValue: function(key, value) {
        AppDispatcher.dispatch({
            type: "settings/set-value",
            data: {
                key,
                value
            }
        });
    },
    updateSetting: function(key, params) {
        AppDispatcher.dispatch({
            type: "settings/update-setting",
            data: {
                key,
                params
            }
        });
    },
    delete: function(key) {
        AppDispatcher.dispatch({
            type: "settings/delete-setting",
            data: {
                key
            }
        });
    },

    valueUpdated: function(key, params) {
        AppDispatcher.dispatch({
            type: "settings/updated-value",
            data: {
                key,
                params
            }
        });
    },
    settingUpdated: function(key, params, option) {
        AppDispatcher.dispatch({
            type: "settings/updated-setting",
            data: {
                key,
                params,
                option
            }
        });
    },
    settingDeleted: function(key) {
        AppDispatcher.dispatch({
            type: "settings/deleted-setting",
            data: {
                key
            }
        });
    },
};
