import AppDispatcher from "../AppDispatcher";

export default {
    loadRooms : function() {
        AppDispatcher.dispatch({
            type: "rooms/load"
        });
    },
    addRoom : function(newRoom) {
        AppDispatcher.dispatch({
            type: "rooms/room-add",
            data: {
                newRoom,
            }
        });
    },
    updateRoom : function(id, newRoom) {
        AppDispatcher.dispatch({
            type: "rooms/room-update",
            data: {
                id,
                newRoom,
            }
        });
    },
    deleteRoom : function(id) {
        AppDispatcher.dispatch({
            type: "rooms/room-delete",
            data: {
                id,
            }
        });
    },
    roomAdded : function(room) {
        AppDispatcher.dispatch({
            type: "rooms/room-added",
            data: room
        });
    },
    roomUpdated : function(id, changed, room) {
        AppDispatcher.dispatch({
            type: "rooms/room-updated",
            data: {
                id,
                changed,
                room
            }
        });
    },
    roomDeleted : function(id) {
        AppDispatcher.dispatch({
            type: "rooms/room-deleted",
            data: { id }
        });
    },
};
