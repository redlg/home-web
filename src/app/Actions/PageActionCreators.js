import AppDispatcher from "../AppDispatcher";

export default {
    setTitle: function(title) {
        AppDispatcher.dispatch({
            type: "page/set-title",
            data: {
                title
            }
        });
    },
    setParam: function(name, value) {
        AppDispatcher.dispatch({
            type: "page/set-value",
            data: {
                name: name,
                value: value
            }
        });
    },
};
