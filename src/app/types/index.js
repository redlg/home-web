'use strict';
// TODO избавиться от DataTypes совсем
import DataTypes from "./DataTypes";
import ItemType from "./ItemType";
import Color from "./Color";
import Lekg from "./Lekg";
import Kettle from "./Kettle";
import Contact from "./Contact";
import Dimmer from "./Dimmer";
import Led from "./Led";
import Number from "./Number";
import Switch from "./Switch";
import Text from "./Text";
import VirtIR from "./VirtIR";
import Guard from "./Guard";

let types = {
    DataTypes,
    ItemType,
    Color,
    Lekg,
    Kettle,
    Contact,
    Dimmer,
    Led,
    Number,
    Switch,
    Text,
    VirtIR,
    Guard,
};
types.generateType = function(type) {
    let obj;
    for(let i in types) {
        if(i == type || i[0].toUpperCase() + i.substr(1) == type) {
            obj = new types[i]();
        }
    }
    if(!obj) {
        obj = new ItemType();
    }
    obj._type = type;

    return obj;
};
export default types;
