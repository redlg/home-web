'use strict';
var DataTypes = require("./DataTypes");

class ItemType {
  constructor(id='', code='', value='') {
    this.id = id;
    this.code = code;
    this.name = code;
    this.value = value;
    this.active = 0;
    this.room_id = null;
    this.options = undefined;
    this.settings = {};
    this.system = 0;
  }
  toJSON() {
    return this._export();
  }
  _export() {
    return {
      _type: this.constructor.name,
      id: this.id,
      code: this.code,
      name: this.name,
      value: this.value,
      last_values: this.last_values,
      active: this.active,
      options: this.options,
      settings: this.settings,
      system: this.system,
      room_id:  this.room_id
    }
  }

  setValues(values) {
    if(typeof values.id !== "undefined") this.id = values.id;
    if(typeof values.code !== "undefined") this.code = values.code;
    if(typeof values.name !== "undefined") this.name = values.name || "<" + values.code + ">";
    if(typeof values.value !== "undefined") this.value = values.value;
    if(typeof values.last_values !== "undefined") this.last_values = values.last_values;
    if(typeof values.active !== "undefined") this.active = values.active;
    if(typeof values.room_id !== "undefined") this.room_id = values.room_id;
    if(typeof values.options !== "undefined") this.options = values.options;
    if(typeof values.settings !== "undefined") this.settings = values.settings;
    if(typeof values.system !== "undefined") this.system = values.system;
    return this;
  }

  // do convert from standard value to internal
  setValue(data) {
    this.value = data;
  }
  setLastValues(data) {
    this.last_values = data;
  }

  // do convert from internal value to standard
  getValue(data) {
    if(typeof this.value === 'string')
      return new DataTypes.Base(this.value);
    else
      return this.value;
  }
}
module.exports = ItemType;
