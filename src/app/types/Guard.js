'use strict';

import Number from "./Number"

export default class Guard extends Number {
    timer = {
        state: 'off',
        time_left: 0,
        time_passed: 0
    }
}
