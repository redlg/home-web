'use strict';

class Base {
  constructor(base) {
    this.value = base;
  }
  setValue(value) {
    this.value = value;
  }
  getValue(value) {
    return this.value;
  }
  _export() {
    return {
      _type: this.constructor.name,
      _value: this.value
    }
  }
}

class OnOff extends Base {
  constructor(base=0) {
    super(base);
  }
}
OnOff.On = 1;
OnOff.Off = 0;

class Percent extends Base {
  constructor(base='0') {
    super(base);
  }
}

class OpenClosed extends Base {
  constructor(base=0) {
    super(base);
  }
}
OpenClosed.Open = 1;
OpenClosed.Closed = 0;

class Decimal extends Base {
  constructor(base='Closed') {
    super(base);
  }
}

class RGB extends Base {
  constructor(base=[0, 0, 0]) {
    super(base);
  }
  setValue(r, g, b) {
    this.value[0] = r;
    this.value[1] = g;
    this.value[2] = b;
  }
  setR(r) {
    this.value[0] = r;
  }
  setG(g) {
    this.value[1] = g;
  }
  setB(b) {
    this.value[2] = b;
  }
}

class LedPercent extends Percent {}
class LedRGB extends RGB {}
class LedGarland extends Base {}
class LedMusic extends Base {}

var datatypes = {
  name: "DataTypes",
  Base: Base,
  OnOff: OnOff,
  RGB: RGB,
  Percent: Percent,
  OpenClosed: OpenClosed,
  Decimal: Decimal,
  LedPercent,
  LedRGB,
  LedGarland,
  LedMusic
};

// datatypes.generateType = function(type) {
//   var value;
//   var generatedType;
//
//   if(typeof type == 'object') {
//     value = type._value;
//     type = type._type;
//   }
//   for(var i in datatypes) {
//     if(i == type || i[0].toUpperCase() + i.substr(1) == type) {
//       generatedType = new datatypes[i](value);
//     }
//   }
//   if(!generatedType)
//     generatedType = new datatypes.Base(value);
//
//   return generatedType;
// };

module.exports = datatypes;
