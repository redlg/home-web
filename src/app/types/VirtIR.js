'use strict';

import ItemType from "./ItemType"
import RemoteEditDialog from "../components/RemoteEditDialog"

export default class VirtIR extends ItemType {
    static dialog = RemoteEditDialog;
}
