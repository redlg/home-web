/**
 * Created by putal_000 on 26.05.2016.
 */
import React, { Component } from "react";
import Subheader from "material-ui/Subheader";
import Toggle from "material-ui/Toggle";
import DeviceSettingsDialog from "../components/DeviceSettingsDialog"
import Dialog from 'material-ui/Dialog';
import TextField from 'material-ui/TextField';
import FlatButton from 'material-ui/FlatButton';

const ItemType = require("./ItemType");

class LekgDialog extends DeviceSettingsDialog {
    handleShowInRoomChange = (event) => {
        let device = this.state.device;
        console.log(event.target);
        device.settings.show_in_room = event.target.checked;
        this.setState({
            device: device
        });
    };
    render() {
        let { dialogActions, ...other } = this.props;

        if(!dialogActions) {
            dialogActions = [
                <FlatButton label="Cancel" primary={true}
                            onTouchTap={this.handleClose} />,
                <FlatButton label="Submit" type="submit" primary={true}
                            onTouchTap={this.handleSave} />
            ];
        }

        return <Dialog actions={dialogActions} {...other}>
            <form onSubmit={this.handleFormSave}>
                <TextField name="device_name"
                           floatingLabelText="Название устройства"
                           value={this.state.device.name || ''}
                           onChange={this.handleNameChange} />
                <Subheader>Options</Subheader>
                <Toggle name="settings[show_in_room]"
                        toggled={!!this.state.device.settings.show_in_room}
                        label="Отображать на странице комнаты"
                        onToggle={this.handleShowInRoomChange} />
            </form>
        </Dialog>;
    }
}
class Lekg extends ItemType {
    static dialog = LekgDialog;
}

export default Lekg;
