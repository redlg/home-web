// base Store class for HOME Stores
import {
    Store
} from "flux/utils";


class HomeStore extends Store {
    constructor(dispatcher) {
        super(dispatcher);
        this.loaded = false;
    }
}

export default HomeStore;
